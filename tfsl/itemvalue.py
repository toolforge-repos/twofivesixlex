"""Holder of the ItemValue class and a function to build one given a JSON representation of it."""

from typing import TYPE_CHECKING, Optional

import tfsl.interfaces as i

if TYPE_CHECKING:
    from typing_extensions import TypeGuard


class ItemValue:
    """Representation of a Wikibase entity ID of some sort."""

    def __init__(self, item_id: i.EntityId) -> None:
        """Sets up an ItemValue.

        Args:
            item_id: ID for the entity being referred to.
        """
        self.type: str
        self.id: i.EntityId
        if i.is_qid(item_id):
            self.type = "item"
            self.id = i.Qid(item_id)
        elif i.is_pid(item_id):
            self.type = "property"
            self.id = i.Pid(item_id)
        elif i.is_lid(item_id):
            self.type = "lexeme"
            self.id = i.Lid(item_id)
        elif i.is_lfid(item_id):
            self.type = "form"
            self.id = i.LFid(item_id)
        elif i.is_lsid(item_id):
            self.type = "sense"
            self.id = i.LSid(item_id)

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two item values.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this item value is equal to the right-hand side.
        """
        if isinstance(rhs, str):
            return self.id == rhs
        elif not isinstance(rhs, ItemValue):
            return NotImplemented
        return self.id == rhs.id and self.type == rhs.type

    def __hash__(self) -> int:
        """Produces a hash for this ItemValue.

        Returns:
            Hash for this ItemValue.
        """
        return hash((self.type, self.id))

    def __str__(self) -> str:
        """Produces a representation of an item value intended for printing to a terminal (not a serialization).

        Returns:
            String representation of an ItemValue.
        """
        return f"{self.id}"

    def __jsonout__(self) -> i.ItemValueDict:
        """Produces a JSON serialization of this ItemValue.

        Returns:
            JSON serialization of this ItemValue.
        """
        base_dict: i.ItemValueDict = {
            "entity-type": self.type,
            "id": self.id,
        }
        if (self.type in ["item", "property", "lexeme"]):
            base_dict["numeric-id"] = int(self.id[1:])
        return base_dict

    def get_qid(self, otherwise: Optional[i.Qid] = None) -> i.Qid:
        """Returns the id in this ItemValue if it is a Qid.

        Args:
            otherwise: What to return if the ItemValue is not a Qid.

        Returns:
            Either the item ID within or the provided otherwise.

        Raises:
            TypeError: if no otherwise is provided and the ItemValue's ID is not a Qid.
        """
        if i.is_qid(self.id):
            return self.id
        elif otherwise is not None:
            return otherwise
        raise TypeError(f"{self.id} is not a Qid")

    def get_pid(self, otherwise: Optional[i.Pid] = None) -> i.Pid:
        """Returns the id in this ItemValue if it is a Pid.

        Args:
            otherwise: What to return if the ItemValue is not a Pid.

        Returns:
            Either the property ID within or the provided otherwise.

        Raises:
            TypeError: if no otherwise is provided and the ItemValue's ID is not a Pid.
        """
        if i.is_pid(self.id):
            return self.id
        elif otherwise is not None:
            return otherwise
        raise TypeError(f"{self.id} is not a Pid")

    def get_lid(self, otherwise: Optional[i.Lid] = None) -> i.Lid:
        """Returns the id in this ItemValue if it is a Lid.

        Args:
            otherwise: What to return if the ItemValue is not an Lid.

        Returns:
            Either the lexeme ID within or the provided otherwise.

        Raises:
            TypeError: if no otherwise is provided and the ItemValue's ID is not a Lid.
        """
        if i.is_lid(self.id):
            return self.id
        elif otherwise is not None:
            return otherwise
        raise TypeError(f"{self.id} is not a Lid")

    def get_lfid(self, otherwise: Optional[i.LFid] = None) -> i.LFid:
        """Returns the id in this ItemValue if it is an LFid.

        Args:
            otherwise: What to return if the ItemValue is not an LFid.

        Returns:
            Either the form ID within or the provided otherwise.

        Raises:
            TypeError: if no otherwise is provided and the ItemValue's ID is not a LFid.
        """
        if i.is_lfid(self.id):
            return self.id
        elif otherwise is not None:
            return otherwise
        raise TypeError(f"{self.id} is not an LFid")

    def get_lsid(self, otherwise: Optional[i.LSid] = None) -> i.LSid:
        """Returns the id in this ItemValue if it is an LSid.

        Args:
            otherwise: What to return if the ItemValue is not an LSid.

        Returns:
            Either the sense ID within or the provided otherwise.

        Raises:
            TypeError: if no otherwise is provided and the ItemValue's ID is not a LSid.
        """
        if i.is_lsid(self.id):
            return self.id
        elif otherwise is not None:
            return otherwise
        raise TypeError(f"{self.id} is not an LSid")


def is_itemvalue(value_in: i.ClaimDictValueDictionary) -> "TypeGuard[i.ItemValueDict]":
    """Checks that the keys expected for an ItemValue exist.

    Args:
        value_in: JSON serialization of some claim value.

    Returns:
        Whether this has the keys of an ItemValue serialization.
    """
    return all(key in value_in for key in ["entity-type", "id"])


def build_itemvalue(value_in: i.ItemValueDict) -> ItemValue:
    """Builds an ItemValue given the Wikibase JSON for one.

    Args:
        value_in: JSON representation of an ItemValue.

    Returns:
        New ItemValue object.
    """
    return ItemValue(value_in["id"])

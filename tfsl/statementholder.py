"""Holds the StatementHolder class and a function to build one given a JSON representation of it."""

from collections import defaultdict
from copy import deepcopy
from textwrap import indent
from typing import Optional

import tfsl.interfaces as i
import tfsl.itemvalue
import tfsl.statement
import tfsl.utils as u


class StatementHolder:
    """Holds a set of statements."""

    def __init__(
        self,
        statements: Optional[i.StatementHolderInput] = None,
        removed_statements: Optional[i.StatementSet] = None,
    ) -> None:
        """Sets up a StatementHolder.

        Args:
            statements: Statements in this holder.
            removed_statements: Statements to be marked for removal.
        """
        super().__init__()

        self.statements = defaultdict(list)
        if isinstance(statements, defaultdict):
            for prop in statements:
                for arg in statements[prop]:
                    self.statements[arg.property].append(arg)
        elif isinstance(statements, list):
            for arg in statements:
                self.statements[arg.property].append(arg)
        if removed_statements is None:
            self.removed_statements = defaultdict(list)
        else:
            self.removed_statements = removed_statements

    def get_statements(self, property_in: i.Pid) -> i.StatementList:
        """Gets statements with a certain property.

        Args:
            property_in: Property to obtain statements for.

        Returns:
            List of statements with the provided property.
        """
        return self.statements.get(property_in, [])

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) in the StatementHolder.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists in the StatementHolder.
        """
        if value_in is None:
            return property_in in self.statements
        elif u.is_novalue(value_in):
            def compare_function(stmt: tfsl.Statement) -> bool:
                """Compares a statement's value to novalue.

                Args:
                    stmt: Statement to compare against.

                Returns:
                    Whether stmt's value is novalue.
                """
                return u.is_novalue(stmt.value)
        elif u.is_somevalue(value_in):
            def compare_function(stmt: tfsl.Statement) -> bool:
                """Compares a statement's value to somevalue.

                Args:
                    stmt: Statement to compare against.

                Returns:
                    Whether stmt's value is somevalue.
                """
                return u.is_somevalue(stmt.value)
        else:
            def compare_function(stmt: tfsl.statement.Statement) -> bool:
                """Compares a statement's value to another value.

                Args:
                    stmt: Statement to compare against.

                Returns:
                    Whether stmt's value and the other value are equal.
                """
                return stmt.value == value_in
        return any(map(compare_function, self.statements[property_in]))

    def __jsonout__(self) -> i.StatementDictSet:
        """Produces a JSON serialization of this StatementHolder.

        Returns:
            JSON serialization of this StatementHolder.
        """
        statement_dict = defaultdict(list)
        for stmtprop, stmtval in self.removed_statements.items():
            statement_dict[stmtprop].extend([stmt.__jsonout__() for stmt in stmtval])
        for stmtprop, stmtval in self.statements.items():
            statement_dict[stmtprop].extend([stmt.__jsonout__() for stmt in stmtval])
        return dict(statement_dict)

    def __len__(self) -> int:
        """Calculates the number of statements in this StatementHolder.

        Returns:
            Size of this StatementHolder.
        """
        return len(self.statements)

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two statement holders.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this statement holder is equal to the right-hand side.
        """
        if isinstance(rhs, StatementHolder):
            return self.statements == rhs.statements
        elif isinstance(rhs, dict):
            return self.statements == rhs
        return NotImplemented

    def __contains__(self, arg: object) -> bool:
        """Checks for the existence of something in a StatementHolder.

        Args:
            arg: Object to check for.

        Returns:
            Whether the argument provided is in the StatementHolder.

        Raises:
            TypeError: if the argument is not something that can be checked in a StatementHolder.
        """
        if isinstance(arg, str):
            if i.is_pid(arg):
                return arg in self.statements
            raise TypeError(f"String {arg} is not a property")
        elif isinstance(arg, tfsl.claim.Claim):
            for prop in self.statements:
                for stmt in self.statements[prop]:
                    if stmt == arg:
                        return True
        elif isinstance(arg, tfsl.statement.Statement):
            return arg in self.statements[arg.property]
        raise TypeError(f"Can't check for {type(arg)} in StatementHolder")

    def __getitem__(self, arg: object) -> i.StatementList:
        """Gets statements with a property from this StatementHolder.

        Args:
            arg: Ideally a property ID.

        Returns:
            List of statements with that property ID.

        Raises:
            KeyError: if arg is not a property ID.
        """
        if isinstance(arg, str) and i.is_pid(arg):
            return self.statements[arg]
        raise KeyError(f"String {arg} is not a property")

    def __str__(self) -> str:
        """Produces a representation of a statement holder intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a StatementHolder.
        """
        if self.statements:
            stmt_list = [str(stmt) for prop in self.statements for stmt in self.statements[prop]]
            return "<\n" + indent("\n".join(stmt_list), tfsl.utils.DEFAULT_INDENT) + "\n>"
        return ""

    def __add__(self, rhs: object) -> "StatementHolder":
        """Adds an object to this StatementHolder.

        Args:
            rhs: Object to add.

        Returns:
            StatementHolder reflecting the added object.

        Raises:
            TypeError: if the object cannot be added to the StatementHolder.
        """
        if not isinstance(rhs, tfsl.statement.Statement):
            raise TypeError(f"Can't add {type(rhs)} to StatementHolder")
        newstmts = deepcopy(self.statements)
        newstmts[rhs.property].append(rhs)
        return StatementHolder(newstmts)

    def __sub__(self, rhs: object) -> "StatementHolder":
        """Removes an object from this StatementHolder.

        Args:
            rhs: Object to remove.

        Returns:
            StatementHolder reflecting the removed object.

        Raises:
            TypeError: if the object cannot be subtracted from a StatementHolder.
        """
        if isinstance(rhs, str):
            if i.is_pid(rhs):
                newstmts = deepcopy(self.statements)
                newremoved = deepcopy(self.removed_statements)
                newremoved[rhs].extend([stmt.set_to_remove() for stmt in newstmts[rhs]])
                del newstmts[rhs]
                return StatementHolder(newstmts, newremoved)
            raise TypeError(f"String {rhs} is not a property")
        elif isinstance(rhs, tfsl.statement.Statement):
            newstmts = deepcopy(self.statements)
            newremoved = deepcopy(self.removed_statements)
            newstmts[rhs.property] = [stmt for stmt in newstmts[rhs.property] if stmt != rhs]
            newremoved[rhs.property].append(rhs.set_to_remove())
            return StatementHolder(newstmts, newremoved)
        raise TypeError(f"Can't subtract {type(rhs)} from StatementHolder")


def build_statement_list(claims_dict: i.StatementDictSet) -> i.StatementSet:
    """Builds a statement set from a JSON dictionary of statements.

    Args:
        claims_dict: JSON serialization of a set of statements.

    Returns:
        New StatementSet.
    """
    claims: i.StatementSet = defaultdict(list)
    for prop in claims_dict:
        for claim in claims_dict[prop]:
            claims[prop].append(tfsl.statement.build_statement(claim))
    return claims


def haswbstatement(statementset: i.StatementDictSet, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
    """Checks for a property (with an optional value) in a StatementDictSet.

    Shamelessly named after the keyword used on Wikidata to look for a statement.

    Args:
        statementset: Set of StatementDicts to check.
        property_in: Property to check for.
        value_in: Value to check for.

    Returns:
        Whether the property (with the value provided) exists in the StatementDictSet.
    """
    if value_in is None:
        return property_in in statementset
    elif tfsl.utils.is_novalue(value_in):
        def compare_function(stmt: i.StatementDict) -> bool:
            """Compares a statement's value to novalue.

            Args:
                stmt: Statement to compare against.

            Returns:
                Whether stmt's value is novalue.
            """
            mainsnak = stmt["mainsnak"]
            return mainsnak["snaktype"] == "novalue"
    elif tfsl.utils.is_somevalue(value_in):
        def compare_function(stmt: i.StatementDict) -> bool:
            """Compares a statement's value to somevalue.

            Args:
                stmt: Statement to compare against.

            Returns:
                Whether stmt's value is somevalue.
            """
            mainsnak = stmt["mainsnak"]
            return mainsnak["snaktype"] == "somevalue"
    else:
        def compare_function(stmt: i.StatementDict) -> bool:
            """Compares a statement's value to another value.

            Args:
                stmt: Statement to compare against.

            Returns:
                Whether stmt's value and the other value are equal.
            """
            mainsnak = stmt["mainsnak"]
            if mainsnak["snaktype"] not in {"novalue", "somevalue"}:
                datavalue = mainsnak["datavalue"]
                return tfsl.claim.build_value(datavalue["value"]) == value_in
            return False
    return any(map(compare_function, statementset.get(property_in, [])))

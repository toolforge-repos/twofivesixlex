"""Holds the Property class and a function to build one given a JSON representation of it."""

from typing import Dict, Optional, Set, Union

import tfsl.auth
import tfsl.interfaces as i
import tfsl.languages
import tfsl.lexemeform
import tfsl.lexemesense
import tfsl.monolingualtext
import tfsl.monolingualtextholder
import tfsl.statement
import tfsl.statementholder
import tfsl.utils


class Property:
    """Container for a Wikidata property."""

    def __init__(
        self,
        datatype: str,
        labels: Optional[Union[tfsl.monolingualtextholder.MonolingualTextHolder, i.MonolingualTextList]] = None,
        descriptions: Optional[Union[tfsl.monolingualtextholder.MonolingualTextHolder, i.MonolingualTextList]] = None,
        aliases: Optional[Dict[i.LanguageCode, Set[str]]] = None,
        statements: Optional[Union[tfsl.statementholder.StatementHolder, i.StatementHolderInput]] = None,
    ) -> None:
        """Sets up a Property.

        Args:
            datatype: Type of the value for this property.
            labels: Initial labels for this property.
            descriptions: Initial descriptions for this property.
            aliases: Initial aliases for this property.
            statements: Initial statements for this property.
        """
        super().__init__()

        self.datatype = datatype

        if isinstance(labels, tfsl.monolingualtextholder.MonolingualTextHolder):
            self.labels = labels
        else:
            self.labels = tfsl.monolingualtextholder.MonolingualTextHolder(labels)

        if isinstance(descriptions, tfsl.monolingualtextholder.MonolingualTextHolder):
            self.descriptions = descriptions
        else:
            self.descriptions = tfsl.monolingualtextholder.MonolingualTextHolder(descriptions)

        if aliases is None:
            self.aliases = {}
        else:
            self.aliases = aliases if isinstance(aliases, dict) else dict(aliases)

        if isinstance(statements, tfsl.statementholder.StatementHolder):
            self.statements = statements
        else:
            self.statements = tfsl.statementholder.StatementHolder(statements)

        self.pageid: Optional[int] = None
        self.namespace: Optional[int] = None
        self.title: Optional[str] = None
        self.lastrevid: Optional[int] = None
        self.modified: Optional[str] = None
        self.type: Optional[str] = None
        self.id: Optional[i.Pid] = None

    def get_published_settings(self) -> i.PropertyPublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the Property JSON dictionary which are only significant at editing time for existing properties.
        """
        if self.pageid is not None and self.namespace is not None and self.title is not None and self.lastrevid is not None and self.modified is not None and self.type is not None and self.id is not None:
            return {
                "pageid": self.pageid,
                "ns": self.namespace,
                "title": self.title,
                "lastrevid": self.lastrevid,
                "modified": self.modified,
                "type": self.type,
                "id": self.id,
            }
        return {}

    def set_published_settings(self, property_in: i.PropertyPublishedSettings) -> None:
        """Sets based on an Property JSON dictionary those variables which are only significant at editing time for existing properties.

        Args:
            property_in: Source statement of relevant editing information.
        """
        if "pageid" in property_in:
            self.pageid = property_in["pageid"]
            self.namespace = property_in["ns"]
            self.title = property_in["title"]
            self.lastrevid = property_in["lastrevid"]
            self.modified = property_in["modified"]
            self.type = property_in["type"]
            self.id = property_in["id"]

    def __getitem__(self, key: object) -> i.StatementList:
        """Gets statements with a property from this Property.

        Args:
            key: Ideally a property ID.

        Returns:
            List of statements with that property ID.

        Raises:
            KeyError: if arg is not a property ID.
        """
        if isinstance(key, str):
            if i.is_pid(key):
                return self.statements[key]
            else:
                raise KeyError
        raise KeyError

    def __str__(self) -> str:
        """Produces a representation of a property intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Property.
        """
        remainder = f"{len(self.labels)}/{len(self.descriptions)}, {len(self.statements)}"
        if self.id:
            return "{" + self.id + ": " + remainder + "}"
        return "{Property: " + remainder + "}"

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the Property.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the Property.
        """
        return self.statements.haswbstatement(property_in, value_in)

    def get_label(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Returns the label on the Property with the provided language.

        Args:
            arg: Language to get.

        Returns:
            Label of the property in that language.
        """
        return self.labels[arg]

    def get_description(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Returns the description on the Property with the provided language.

        Args:
            arg: Language to get.

        Returns:
            Description of the property in that language.
        """
        return self.descriptions[arg]

    def __add__(self, arg: object) -> "Property":
        """Adds an object to this Property.

        Args:
            arg: Object to add.

        Returns:
            Property reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be properly added to the Property.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            property_out = Property(
                self.datatype, self.labels, self.descriptions, self.aliases,
                self.statements + arg,
            )
            property_out.set_published_settings(published_settings)
            return property_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            raise NotImplementedError("Adding MonolingualText to Property is ambiguous")
        raise NotImplementedError(f"Can't add {type(arg)} to Property")

    def add_label(self, arg: tfsl.monolingualtext.MonolingualText) -> "Property":
        """Adds the provided MonolingualText as a label to the Property, overwriting any existing label in that MonolingualText's language.

        Args:
            arg: MonolingualText to add.

        Returns:
            Property with the noted language label added.
        """
        published_settings = self.get_published_settings()
        property_out = Property(
            self.datatype, self.labels + arg, self.descriptions, self.aliases,
            self.statements,
        )
        property_out.set_published_settings(published_settings)
        return property_out

    def add_description(self, arg: tfsl.monolingualtext.MonolingualText) -> "Property":
        """Adds the provided MonolingualText as a description to the Property, overwriting any existing description in that MonolingualText's language.

        Args:
            arg: MonolingualText to add.

        Returns:
            Property with the noted language description added.
        """
        published_settings = self.get_published_settings()
        property_out = Property(
            self.datatype, self.labels, self.descriptions + arg, self.aliases,
            self.statements,
        )
        property_out.set_published_settings(published_settings)
        return property_out

    def __sub__(self, arg: object) -> "Property":
        """Removes an object from this Property.

        Args:
            arg: Object to remove.

        Returns:
            Property reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from a Property.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            property_out = Property(
                self.datatype, self.labels, self.descriptions, self.aliases,
                self.statements - arg,
            )
            property_out.set_published_settings(published_settings)
            return property_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            raise NotImplementedError("Subtracting MonolingualText from Property is ambiguous")
        raise NotImplementedError(f"Can't subtract {type(arg)} from Lexeme")

    def sub_label(self, arg: i.LanguageOrMT) -> "Property":
        """Removes the label with the provided language (or the language of the provided MonolingualText) from the Property.

        Args:
            arg: Language or MonolingualText (whose language will be used).

        Returns:
            Property with the noted language label removed.
        """
        published_settings = self.get_published_settings()
        property_out = Property(
            self.datatype, self.labels - arg, self.descriptions, self.aliases,
            self.statements,
        )
        property_out.set_published_settings(published_settings)
        return property_out

    def sub_description(self, arg: i.LanguageOrMT) -> "Property":
        """Removes the description with the provided language (or the language of the provided MonolingualText) from the Property.

        Args:
            arg: Language or MonolingualText (whose language will be used).

        Returns:
            Property with the noted language description removed.
        """
        published_settings = self.get_published_settings()
        property_out = Property(
            self.datatype, self.labels, self.descriptions - arg, self.aliases,
            self.statements,
        )
        property_out.set_published_settings(published_settings)
        return property_out


def build_property(property_in: i.PropertyDict) -> Property:
    """Builds a Property from the JSON dictionary describing it.

    Args:
        property_in: JSON representation of a Property.

    Returns:
        New Property object.
    """
    labels = tfsl.monolingualtextholder.build_text_list(property_in["labels"])
    descriptions = tfsl.monolingualtextholder.build_text_list(property_in["descriptions"])
    statements = tfsl.statementholder.build_statement_list(property_in["claims"])

    aliases: Dict[i.LanguageCode, Set[str]] = {}
    for lang, aliaslist in property_in["aliases"].items():
        aliases[lang] = set()
        for alias in aliaslist:
            new_alias = alias["value"]  # @ tfsl.languages.get_first_lang(alias["language"])
            aliases[lang].add(new_alias)

    property_out = Property(property_in["datatype"], labels, descriptions, aliases, statements)
    property_out.set_published_settings(property_in)
    return property_out


def retrieve_property_json(pid_in: Union[int, i.Pid]) -> i.PropertyDict:
    """Retrieves the JSON for the property with the given Pid.

    Args:
        pid_in: Representation of a property ID.

    Returns:
        JSON for that property.

    Raises:
        ValueError: if the returned JSON for the given Pid is not for a Property.
    """
    pid = i.get_pid_string(pid_in)
    property_dict = tfsl.auth.retrieve_single_entity(pid)
    if i.is_propertydict(property_dict):
        return property_dict
    raise ValueError(f"Returned JSON for {pid_in} is not a property")


class P:
    """A Property, but labels/descriptions are not auto-converted to MonolingualTexts and statements are only assembled into Statements when accessed."""

    def __init__(self, input_arg: Union[int, i.Pid]) -> None:
        """Sets up a P.

        Args:
            input_arg: Indicator of a property ID.
        """
        self.property_json: i.PropertyDict = retrieve_property_json(input_arg)

    def get_label(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the label with the given language code.

        Args:
            lang: Language to find.

        Returns:
            MonolingualText with the property's label in the given language.
        """
        label_dict: i.LemmaDict = self.property_json["labels"][lang.code]
        return label_dict["value"] @ lang

    def get_description(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the description with the given language code.

        Args:
            lang: Language to find.

        Returns:
            MonolingualText with the property's description in the given language.
        """
        description_dict: i.LemmaDict = self.property_json["descriptions"][lang.code]
        return description_dict["value"] @ lang

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the property with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return [tfsl.statement.build_statement(stmt) for stmt in self.property_json["claims"].get(prop, [])]

    def __getitem__(self, prop: i.Pid) -> i.StatementList:
        """Gets statements with a property from this Property.

        Args:
            prop: Ideally a property ID.

        Returns:
            List of statements with that property ID.
        """
        return self.get_stmts(prop)

"""Holds the StatementHolder class and a function to build one given a JSON representation of it."""

from copy import deepcopy
from functools import singledispatchmethod
from typing import Callable, Iterator, Optional

import tfsl.interfaces as i
import tfsl.languages
import tfsl.monolingualtext


def rep_language_is(desired_language: tfsl.languages.Language) -> Callable[[tfsl.monolingualtext.MonolingualText], bool]:
    """Returns a function checking that the provided MonolingualText is in a certain language.

    Args:
        desired_language: Language to check for.

    Returns:
        Function checking that the language of a MonolingualText is the desired language.
    """
    def is_desired_language(text: tfsl.monolingualtext.MonolingualText) -> bool:
        """Checks the language of a MonolingualText.

        Args:
            text: MonolingualText to check.

        Returns:
            Whether text's language is the desired language.
        """
        return text.language == desired_language
    return is_desired_language


class MonolingualTextHolder:
    """Holds a set of strings with languages attached to them."""

    def __init__(
        self,
        texts: Optional[i.MonolingualTextHolderInput] = None,
        removed_texts: Optional[i.MonolingualTextList] = None,
    ) -> None:
        """Sets up a MonolingualTextHolder.

        Args:
            texts: MonolingualTexts in this holder.
            removed_texts: MonolingualTexts to be marked for removal.
        """
        super().__init__()

        self.texts: i.MonolingualTextList
        if isinstance(texts, tfsl.monolingualtext.MonolingualText):
            self.texts = [texts.text @ texts.language]
        elif texts is None:
            self.texts = []
        else:
            self.texts = texts

        if removed_texts is None:
            self.removed_texts: i.MonolingualTextList = []
        else:
            self.removed_texts = removed_texts

    def __jsonout__(self) -> i.LemmaDictSet:
        """Produces a JSON serialization of this MonolingualTextHolder.

        Returns:
            JSON serialization of this MonolingualTextHolder.
        """
        base_dict: i.LemmaDictSet = {text.language.code: {"value": text.text, "language": text.language.code, "remove": ""} for text in self.removed_texts}
        for text in self.texts:
            base_dict[text.language.code] = {"value": text.text, "language": text.language.code}
        return base_dict

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two monolingual text holders.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this monolingual text holder is equal to the right-hand side.
        """
        if isinstance(rhs, MonolingualTextHolder):
            return self.texts == rhs.texts
        elif isinstance(rhs, list):
            return set(self.texts) == set(rhs)
        return NotImplemented

    def __contains__(self, arg: object) -> bool:
        """Checks for something inside the MonolingualTextHolder.

        Args:
            arg: Object to check for.

        Returns:
            Whether the object is inside the MonolingualTextHolder.
        """
        return self.contains(arg)

    def __len__(self) -> int:
        """Calculates the number of entries in this MonolingualTextHolder.

        Returns:
            Size of this MonolingualTextHolder.
        """
        return len(self.texts)

    def __iter__(self) -> Iterator[tfsl.monolingualtext.MonolingualText]:
        """Allows the MonolingualTexts to be iterated over.

        Returns:
            Iterator over the MonolingualTexts in the Holder.
        """
        return iter(self.texts)

    @singledispatchmethod
    def contains(self, arg: object) -> bool:
        """Dispatches __contains__.

        Args:
            arg: Something to check for.

        Returns:
            Whether that object is in the MonolingualTextHolder.

        Raises:
            TypeError: if the argument can't be checked for in a MonolingualTextHolder.
        """
        raise TypeError(f"Can't check for {type(arg)} in MonolingualTextHolder")

    @contains.register
    def _(self, arg: tfsl.languages.Language) -> bool:
        """Checks for a language in the MonolingualTextHolder.

        Args:
            arg: Language to check for.

        Returns:
            Whether a MonolingualText with the given language is in the holder.
        """
        return any((text.language == arg) for text in self.texts)

    @contains.register
    def _(self, arg: tfsl.monolingualtext.MonolingualText) -> bool:
        """Checks for the language of the provided MonolingualText in the MonolingualTextHolder.

        Args:
            arg: Language to check for.

        Returns:
            Whether a MonolingualText with arg's language is in the holder.
        """
        return arg in self.texts

    def __getitem__(self, arg: object) -> tfsl.monolingualtext.MonolingualText:
        """Gets MonolingualTexts from this MonolingualTextHolder.

        Args:
            arg: Indicator of something to get from a MonolingualTextHolder.

        Returns:
            MonolingualText corresponding to the argument.
        """
        return self.get_mt(arg)

    @singledispatchmethod
    def get_mt(self, arg: object) -> tfsl.monolingualtext.MonolingualText:
        """Dispatches __getitem__.

        Args:
            arg: Something to get from a MonolingualTextHolder.

        Returns:
            MonolingualText corresponding to the arg.

        Raises:
            TypeError: if the argument can't be checked for in a MonolingualTextHolder.
        """
        raise TypeError(f"Can't get {type(arg)} from MonolingualTextHolder")

    @get_mt.register
    def _(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Obtains a MonolingualText from the holder in the desired language.

        Args:
            arg: Language to look for.

        Returns:
            MonolingualText with the desired language.
        """
        return next(filter(rep_language_is(arg), self.texts))

    @get_mt.register
    def _(self, arg: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText:
        """Obtains a MonolingualText from the holder in the language of the given MonolingualText.

        Args:
            arg: MonolingualText whose language will be looked for.

        Returns:
            MonolingualText with the desired language.
        """
        return next(filter(lambda text: text == arg, self.texts))

    def __str__(self) -> str:
        """Produces a representation of a monolingual text holder intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a MonolingualTextHolder.
        """
        return " / ".join([f"{text.text}@{text.language.code}" for text in self.texts])

    def __add__(self, rhs: object) -> "MonolingualTextHolder":
        """Adds an object to this MonolingualTextHolder.

        Args:
            rhs: Object to add.

        Returns:
            MonolingualTextHolder reflecting the added object.

        Raises:
            TypeError: if the object cannot be added to the MonolingualTextHolder.
        """
        if isinstance(rhs, tfsl.monolingualtext.MonolingualText):
            newtexts = deepcopy(self.texts)
            newtexts = [rep for rep in newtexts if rep.language != rhs.language]
            newtexts.append(rhs)
            return MonolingualTextHolder(newtexts)
        raise TypeError(f"Can't add {type(rhs)} to MonolingualTextHolder")

    def __sub__(self, rhs: object) -> "MonolingualTextHolder":
        """Removes an object from this MonolingualTextHolder.

        Args:
            rhs: Object to remove.

        Returns:
            MonolingualTextHolder reflecting the removed object.

        Raises:
            TypeError: if the object cannot be subtracted from a MonolingualTextHolder.
        """
        if isinstance(rhs, tfsl.languages.Language):
            newtexts = []
            for rep in self.texts:
                if rep.language == rhs:
                    self.removed_texts.append(rep)
                else:
                    newtexts.append(rep)
            return MonolingualTextHolder(newtexts, self.removed_texts)
        elif isinstance(rhs, tfsl.monolingualtext.MonolingualText):
            newtexts = [rep for rep in self.texts if rep != rhs]
            if rhs in self.texts:
                self.removed_texts.append(rhs)
            return MonolingualTextHolder(newtexts, self.removed_texts)
        raise TypeError(f"Can't subtract {type(rhs)} from MonolingualTextHolder")


def build_text_list(text_dict: i.LemmaDictSet) -> i.MonolingualTextList:
    """Builds a statement set from a JSON dictionary of statements.

    Args:
        text_dict: JSON serialization of a set of MonolingualTexts.

    Returns:
        New list of MonolingualTexts.
    """
    texts: i.MonolingualTextList = []
    for _, text in text_dict.items():
        new_text = text["value"] @ tfsl.languages.get_first_lang(text["language"])
        texts.append(new_text)
    return texts


def get_lang_from_mtlist(mtlist: i.LemmaDictSet, language: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
    """Returns a MonolingualText from a LemmaDictSet based on the language code of the provided Language.

    Args:
        mtlist: Set of JSON serializations of monolingual texts.
        language: Language to obtain therefrom.

    Returns:
        MonolingualText in the desired language.
    """
    lang_code = language.code
    return tfsl.monolingualtext.build_lemma(mtlist[lang_code])

"""Holds the WikibaseSession class and other functionality related to network accesses."""

import configparser
import json
import logging
import os
import time
from getpass import getpass
from itertools import islice
from pathlib import Path
from typing import Any, Dict, Iterable, Iterator, List, Optional, Tuple, Union

import requests

import tfsl.interfaces as i

maxlag: int = 5

token_request_params = {
    "action": "query",
    "meta": "tokens",
    "type": "login",
    "format": "json",
}

csrf_token_params = {
    "action": "query",
    "meta": "tokens",
    "format": "json",
}

WIKIDATA_API_URL = "https://www.wikidata.org/w/api.php"
DEFAULT_USER_AGENT = "tfsl 0.0.1"


class WikibaseSession:
    """Auth library for Wikibases."""

    def __init__(
        self,
        username: str,
        password: Optional[str] = None,
        token: Optional[str] = None,
        user_agent: str = DEFAULT_USER_AGENT,
        url: str = WIKIDATA_API_URL,
    ) -> None:
        """Sets up a WikibaseSession.

        Args:
            username: Username of the user logging in.
            password: Password of the user logging in.
            token: Login token to be used when logging in.
            user_agent: User agent to be used in session requests.
            url: Endpoint for session requests.

        Raises:
            PermissionError: if the login attempt fails.
        """
        self.url = url
        self.user_agent = user_agent
        self.headers = {"User-Agent": user_agent}
        self.session = requests.Session()

        self.username = username
        self.assert_user = None
        if password is not None:
            self._password = password
        else:
            self._password = getpass(f"Enter password for {self.username}: ")

        token_response = self.get(token_request_params)
        login_token = token_response["query"]["tokens"]["logintoken"]

        connection_request_params = {
            "action": "login",
            "format": "json",
            "lgname": self.username,
            "lgpassword": self._password,
            "lgtoken": login_token,
        }
        connection_response = self.post(connection_request_params, 30)
        if connection_response.get("login", []).get("result") != "Success":
            raise PermissionError("Login failed", connection_response["login"]["reason"])
        logging.info("Log in succeeded")

        if token is not None:
            self.csrf_token = token
            logging.info("Using CSRF token: %s", self.csrf_token)
        else:
            csrf_response = self.get(csrf_token_params)
            self.csrf_token = csrf_response["query"]["tokens"]["csrftoken"]
            logging.info("Got CSRF token: %s", self.csrf_token)

        if username is not None:
            # truncate bot name if a "bot password" is used
            self.assert_user = username.split("@")[0]

    def push(self, obj_in: i.Entity, summary: Optional[str] = None, maxlag_in: int = maxlag, bot: bool = False) -> Any:
        """Post data to Wikibase.

        Args:
            obj_in: Entity to submit data for.
            summary: Edit summary for the change being made.
            maxlag_in: Maxlag to send to the server.
            bot: Whether the edit should use the bot flag.

        Returns:
            Response from the API.

        Raises:
            Exception: if a response other than 200 is returned by the server.
            PermissionError: if the API returns some error message.
        """
        data = obj_in.__jsonout__()
        requestjson = {"action": "wbeditentity", "format": "json"}
        if bot:
            requestjson["bot"] = ""

        if not data.get("id", False):
            if data.get("lexicalCategory", False):
                requestjson["new"] = "lexeme"
            elif data.get("glosses", False):
                requestjson["new"] = "sense"
            elif data.get("representations", False):
                requestjson["new"] = "form"
            elif data.get("labels", False) and data.get("sitelinks", False):
                requestjson["new"] = "item"
            else:
                requestjson["new"] = "property"
        else:
            requestjson["id"] = data["id"]

        if summary is not None:
            requestjson["summary"] = summary
        if self.assert_user is not None:
            requestjson["assertuser"] = self.assert_user

        requestjson["token"] = self.csrf_token
        requestjson["data"] = json.dumps(data)
        requestjson["maxlag"] = str(maxlag_in)

        push_response = self.session.post(
            self.url,
            data=requestjson, headers=self.headers, auth=None,
        )
        if push_response.status_code != 200:
            error_msg = f"POST unsuccessful ({push_response.status_code}): {push_response.text}"
            raise Exception(error_msg)

        push_response_data = push_response.json()
        if "error" in push_response_data:
            if push_response_data["error"]["code"] == "maxlag":
                sleepfor = float(push_response.headers.get("retry-after", 5))
                logging.info("Maxlag hit, waiting for %.1f seconds", sleepfor)
                time.sleep(sleepfor)
                return self.push(obj_in)
            else:
                raise PermissionError("API returned error: " + str(push_response_data["error"]))

        logging.debug("Post request succeed")
        return push_response_data

    def post(self, data: Dict[str, str], maxlag_in: int = maxlag) -> Any:
        """Post data to Wikibase. The CSRF token is automatically filled in if __AUTO__ is given instead.

        Args:
            data: Parameters to send via POST
            maxlag_in: Maxlag value to send to the server.

        Returns:
            Answer from the server

        Raises:
            Exception: if a response other than 200 is returned by the server.
            PermissionError: if the API returns some error message.
        """
        if data.get("token") == "__AUTO__":
            data["token"] = self.csrf_token
        if "assertuser" not in data and self.assert_user is not None:
            data["assertuser"] = self.assert_user
        data["maxlag"] = str(maxlag_in)

        post_response = self.session.post(self.url, data=data, headers=self.headers, auth=None)
        if post_response.status_code != 200:
            error_msg = f"POST unsuccessful ({post_response.status_code}): {post_response.text}"
            raise Exception(error_msg)

        post_response_data = post_response.json()
        if "error" in post_response_data:
            if post_response_data["error"]["code"] == "maxlag":
                sleepfor = float(post_response.headers.get("retry-after", 5))
                logging.info("Maxlag hit, waiting for %.1f seconds", sleepfor)
                time.sleep(sleepfor)
                return self.post(data)
            else:
                raise PermissionError("API returned error: " + str(post_response_data["error"]))

        logging.debug("Post request succeed")
        return post_response_data

    def get(self, data: Dict[str, str]) -> Any:
        """Send a GET request to Wikibase.

        Args:
            data: Parameters to send via GET

        Returns:
            Answer from the server

        Raises:
            Exception: if a response other than 200 is returned by the server or if the API returns some error message.
        """
        get_response = self.session.get(self.url, params=data, headers=self.headers)
        get_response_data = get_response.json()
        if get_response.status_code != 200 or "error" in get_response_data:
            # We do not set maxlag for GET requests – so this error can only
            # occur if the users sets maxlag in the request data object
            if get_response_data["error"]["code"] == "maxlag":
                sleepfor = float(get_response.headers.get("retry-after", 5))
                logging.info("Maxlag hit, waiting for %.1f seconds", sleepfor)
                time.sleep(sleepfor)
                return self.get(data)
            else:
                raise Exception(f"GET unsuccessful ({get_response.status_code}): {get_response.text}")
        logging.debug("Get request succeed")
        return get_response_data


def get_wikidata_entities(lids: List[i.EntityId], user_agent: str = DEFAULT_USER_AGENT) -> Dict[i.EntityId, i.EntityPublishedSettings]:
    """Retrieves a list of entities using the Wikidata APi.

    Args:
        lids: List of entity IDs.
        user_agent: User agent to use in requests.

    Returns:
        Mapping of entity IDs to their JSON representations.

    Raises:
        ValueError: if the API response is not valid JSON
        PermissionError: if a response other than 200 is returned by the server or if the API returns some error message.
    """
    query_parameters = {
        "action": "wbgetentities",
        "format": "json",
        "ids": "|".join(lids),
    }
    current_headers = {
        "User-Agent": user_agent,
    }
    get_response = requests.get(WIKIDATA_API_URL, params=query_parameters, headers=current_headers, timeout=5)
    data_output = get_response.json()
    if get_response.status_code != 200 or "error" in data_output:
        raise PermissionError("API returned error: " + str(data_output["error"]))
    if isinstance(data_output, dict):
        returned_entities: Dict[i.EntityId, i.EntityPublishedSettings] = data_output["entities"]
        return returned_entities
    raise ValueError(f"Response from retrieving {lids} not valid JSON")


def read_config() -> Tuple[str, float]:
    """Reads the config file residing at /path/to/tfsl/config.ini.

    Returns:
        Path to entity cache and time-to-live for that cache.
    """
    config = configparser.ConfigParser()
    current_config_path = (Path(__file__).parent / "../config.ini").resolve()
    config.read(current_config_path)
    cpath = config["Tfsl"]["CachePath"]
    ttl = float(config["Tfsl"]["TimeToLive"])
    return cpath, ttl


cache_path, time_to_live = read_config()
os.makedirs(cache_path, exist_ok=True)


def get_filename(entity_name: str) -> str:
    """Constructs the name of a text file containing a sense subgraph based on a given property.

    Args:
        entity_name: Identifier for an entity.

    Returns:
        Path to where that entity's data should be stored.
    """
    return os.path.join(cache_path, f"{entity_name}.json")


def retrieve_single_entity(entity: Union[i.Qid, i.Pid, i.Lid]) -> i.EntityPublishedSettings:
    """Retrieves the JSON for a single Wikibase entity.

    Args:
        entity: ID for the entity to be retrieved.

    Returns:
        Response from the API.

    Raises:
        AssertionError: (handled internally) if the edit time of the cached entity data is less than the time to live.
        ValueError: if the API response is not the JSON representation of an entity.

    Todo:
        Handle retrieving senses and forms
    """
    filename = get_filename(entity)
    try:
        if time.time() - os.path.getmtime(filename) >= time_to_live:
            raise AssertionError
        with os.fdopen(os.open(filename, os.O_RDONLY), encoding="utf-8") as fileptr:
            current_output = json.load(fileptr)
    except (OSError, AssertionError):
        current_entities = get_wikidata_entities([entity])
        current_output = current_entities[entity]
        with os.fdopen(os.open(filename, os.O_WRONLY | os.O_CREAT), "w", encoding="utf-8") as fileptr:
            json.dump(current_output, fileptr)
    if not i.is_entitypublishedsettings(current_output):
        raise ValueError(f"Retrieved data for {entity} was not an entity")
    return current_output


def batched(iterable: Iterable, n: int) -> Iterator[Tuple]:
    """Batch data into tuples of length n. The last batch may be shorter.

    Recipe from https://docs.python.org/3.11/library/itertools.html#itertools-recipes

    Args:
        iterable: Some iterable to be batched.
        n: Maximum length of each batch.

    Yields:
        Batch of (at most) n values from the iterable.

    Raises:
        ValueError: if n is not at least 1.

    Todo:
        simply reimplement this recipe wherever it must occur?
    """
    if n < 1:
        raise ValueError("n must be at least one")
    it = iter(iterable)
    while batch := tuple(islice(it, n)):
        yield batch


def retrieve_multiple_entities(entities: List[Union[i.Qid, i.Pid, i.Lid]]) -> List[i.EntityPublishedSettings]:
    """Retrieves the JSON for multiple Wikibase entities.

    Args:
        entities: List of entity IDs to get JSON representations for.

    Returns:
        List of those JSON representations.

    Raises:
        AssertionError: (handled internally) if the edit time of the cached entity data is less than the time to live.
    """
    retrieved_entities = {}
    still_to_retrieve = set()
    for entity in entities:
        filename = get_filename(entity)
        try:
            if time.time() - os.path.getmtime(filename) >= time_to_live:
                raise AssertionError
            with open(filename, encoding="utf-8") as fileptr:
                current_output = json.load(fileptr)
            retrieved_entities[entity] = current_output
        except (OSError, AssertionError):
            still_to_retrieve.add(entity)

    for remaining_selection in batched(list(still_to_retrieve), 50):
        new_entities = get_wikidata_entities(remaining_selection)
        for entity, current_output in new_entities.items():
            filename = get_filename(entity)
            with os.fdopen(os.open(filename, os.O_WRONLY | os.O_CREAT), "w", encoding="utf-8") as fileptr:
                json.dump(current_output, fileptr)
        retrieved_entities |= new_entities

    return retrieved_entities

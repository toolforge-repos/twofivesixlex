"""Holder of the Claim class and a function to build one given a JSON representation of it."""

from typing import Dict, Optional, Union, overload

import tfsl.coordinatevalue
import tfsl.interfaces as i
import tfsl.itemvalue
import tfsl.monolingualtext
import tfsl.quantityvalue
import tfsl.timevalue
import tfsl.utils

type_string_to_type: Dict[str, type] = {
    "string": str,
    "globecoordinate": tfsl.coordinatevalue.CoordinateValue,
    "monolingualtext": tfsl.monolingualtext.MonolingualText,
    "quantity": tfsl.quantityvalue.QuantityValue,
    "time": tfsl.timevalue.TimeValue,
    "wikibase-entityid": tfsl.itemvalue.ItemValue,
}


class Claim:
    """Representation of a claim, or a property-predicate pair.

    These may be added to statements directly, as qualifiers, or as parts of references.
    """

    def __init__(self, property_in: i.Pid, value: i.ClaimValue) -> None:
        """Sets up a Claim.

        Args:
            property_in: Property of this Claim.
            value: Value of this Claim.

        Raises:
            TypeError: if the type of the value does not match the type expected by the property.
        """
        self.property: i.Pid = property_in
        self.value: i.ClaimValue
        self.datatype: str = tfsl.utils.values_datatype(self.property)
        if tfsl.utils.is_novalue(value) or tfsl.utils.is_somevalue(value):
            self.value = value
        else:
            value_type = type(value)
            property_type = type_string_to_type[tfsl.utils.external_to_internal_type_mapping[self.datatype]]
            if property_type == value_type:
                self.value = value
            else:
                raise TypeError(f"Providing {value_type} as {property_in} value where {property_type} expected")

        self.snaktype: Optional[str] = None
        self.hash: Optional[str] = None

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two claims.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this claim is equal to the right-hand side.
        """
        if not isinstance(rhs, Claim):
            return NotImplemented
        return self.property == rhs.property and self.value == rhs.value

    def __hash__(self) -> int:
        """Produces a hash for this Claim.

        Returns:
            Hash for this Claim.
        """
        return hash((self.property, self.value))

    def __str__(self) -> str:
        """Produces a representation of a claim intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Claim.
        """
        return f"{self.property}: {self.value}"

    def __jsonout__(self) -> i.ClaimDict:
        """Produces a JSON serialization of this Claim.

        Returns:
            JSON serialization of this Claim.
        """
        snaktype: str = "value"
        value_out: Optional[i.ClaimDictValue] = None
        datavalue_out: Optional[i.ClaimDictDatavalue] = None

        if isinstance(self.value, bool):
            if self.value is False:
                snaktype = "novalue"
            elif self.value is True:
                snaktype = "somevalue"
        elif isinstance(self.value, str):
            value_out = self.value
        else:
            value_out = self.value.__jsonout__()
        if value_out is not None:
            datavalue_out = {
                "value": value_out,
                "type": tfsl.utils.values_type(self.property),
            }

        claimdict_out: i.ClaimDict = {
            "snaktype": snaktype,
            "property": self.property,
            "datatype": tfsl.utils.values_datatype(self.property),
        }
        if datavalue_out is not None:
            claimdict_out["datavalue"] = datavalue_out
        return claimdict_out

    def get_itemvalue(
        self,
        novalue: Optional[tfsl.itemvalue.ItemValue] = None,
        somevalue: Optional[tfsl.itemvalue.ItemValue] = None,
    ) -> tfsl.itemvalue.ItemValue:
        """Gets an ItemValue from the claim.

        Args:
            novalue: Value to return if the claim's value is novalue.
            somevalue: Value to return if the claim's value is somevalue.

        Returns:
            Claim's value if it is an ItemValue, or the 'somevalue' argument if the claim's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the statement did not yield an ItemValue.
        """
        if isinstance(self.value, tfsl.itemvalue.ItemValue):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield an ItemValue")

    def get_monolingualtext(
        self,
        novalue: Optional[tfsl.monolingualtext.MonolingualText] = None,
        somevalue: Optional[tfsl.monolingualtext.MonolingualText] = None,
    ) -> tfsl.monolingualtext.MonolingualText:
        """Gets a MonolingualText from the claim.

        Args:
            novalue: Value to return if the claim's value is novalue.
            somevalue: Value to return if the claim's value is somevalue.

        Returns:
            Claim's value if it is a MonolingualText, or the 'somevalue' argument if the claim's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the statement did not yield a MonolingualText.
        """
        if isinstance(self.value, tfsl.monolingualtext.MonolingualText):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield a MonolingualText")

    def get_str(
        self,
        novalue: Optional[str] = None,
        somevalue: Optional[str] = None,
    ) -> str:
        """Gets a string from the claim.

        Args:
            novalue: Value to return if the claim's value is novalue.
            somevalue: Value to return if the claim's value is somevalue.

        Returns:
            Claim's value if it is a string, or the 'somevalue' argument if the claim's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the claim did not yield a string.
        """
        if isinstance(self.value, str):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield a string")


@overload
def build_value(actual_value: i.QuantityValueDict) -> tfsl.quantityvalue.QuantityValue: ...
@overload
def build_value(actual_value: i.MonolingualTextDict) -> tfsl.monolingualtext.MonolingualText: ...
@overload
def build_value(actual_value: i.CoordinateValueDict) -> tfsl.coordinatevalue.CoordinateValue: ...
@overload
def build_value(actual_value: i.TimeValueDict) -> tfsl.timevalue.TimeValue: ...
@overload
def build_value(actual_value: i.ItemValueDict) -> tfsl.itemvalue.ItemValue: ...
@overload
def build_value(actual_value: str) -> str: ...


def build_value(actual_value: Union[str, i.ClaimDictValueDictionary]) -> i.ClaimValue:
    """Builds a ClaimValue given the Wikibase JSON for one.

    Args:
        actual_value: Serialized representation of the value of a claim.

    Returns:
        Object for the value in question.

    Raises:
        ValueError: if an unsupported type of value was passed into this function.
    """
    if isinstance(actual_value, str):
        return actual_value
    elif tfsl.itemvalue.is_itemvalue(actual_value):
        return tfsl.itemvalue.build_itemvalue(actual_value)
    elif tfsl.monolingualtext.is_mtvalue(actual_value):
        return tfsl.monolingualtext.build_mtvalue(actual_value)
    elif tfsl.coordinatevalue.is_coordinatevalue(actual_value):
        return tfsl.coordinatevalue.build_coordinatevalue(actual_value)
    elif tfsl.quantityvalue.is_quantityvalue(actual_value):
        return tfsl.quantityvalue.build_quantityvalue(actual_value)
    elif tfsl.timevalue.is_timevalue(actual_value):
        return tfsl.timevalue.build_timevalue(actual_value)
    raise ValueError("Attempting to build value of unsupported type")


def build_claim(claim_in: i.ClaimDict) -> Claim:
    """Builds a Claim given the Wikibase JSON for one.

    Args:
        claim_in: JSON representation of a claim.

    Returns:
        New Claim object.
    """
    claim_prop: i.Pid
    claim_value: i.ClaimValue

    claim_prop = claim_in["property"]
    if claim_in["snaktype"] == "novalue":
        claim_value = False
    elif claim_in["snaktype"] == "somevalue":
        claim_value = True
    else:
        claim_datavalue = claim_in["datavalue"]
        claim_value = build_value(claim_datavalue["value"])

    claim_out = Claim(claim_prop, claim_value)
    claim_out.snaktype = claim_in["snaktype"]
    claim_out.hash = claim_in["hash"]
    claim_out.datatype = claim_in["datatype"]
    return claim_out

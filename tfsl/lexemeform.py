"""Holds the LexemeForm class and a function to build one given a JSON representation of it."""

from functools import singledispatchmethod
from typing import Collection, List, Optional, Protocol, Set, Union, overload

import tfsl.interfaces as i
import tfsl.languages
import tfsl.monolingualtext
import tfsl.monolingualtextholder as mth
import tfsl.statement
import tfsl.statementholder as sth
import tfsl.utils


class LexemeFormLike(i.MTST, Protocol):
    """Defines methods that may be expected when reading from an object representing a Wikibase lexeme form, whether this object is editable (LexemeForm) or not (LF).

    Attributes:
        features: Grammatical features of this form.
        representations: Representations of this form.
        id: Identifier for this form.
    """
    @property
    def features(self) -> Collection[i.Qid]:
        """Returns the form's grammatical features."""
    @property
    def representations(self) -> mth.MonolingualTextHolder:
        """Returns the form's representations."""
    @property
    def id(self) -> Optional[str]:
        """Returns the form's LFid."""

    def to_editable(self) -> "LexemeForm":
        """Returns an editable version of the form."""

    def __jsonout__(self) -> i.LexemeFormDict:
        """Produces a JSON serialization of this form-like object.

        Returns:
            JSON serialization of this form-like object.
        """


class LexemeForm:
    """Container for a Wikidata lexeme form.

    See the documentation of LexemeFormLike methods for general information about
    what certain methods do.
    """

    def __init__(
        self,
        representations: Union[
            mth.MonolingualTextHolder,
            tfsl.monolingualtext.MonolingualText,
            i.MonolingualTextList,
        ],
        features: Optional[Union[List[i.Qid], Set[i.Qid]]] = None,
        statements: Optional[Union[sth.StatementHolder, i.StatementHolderInput]] = None,
    ) -> None:
        """Sets up a LexemeForm.

        Args:
            representations: Initial form representations.
            features: Initial grammatical features for this form.
            statements: Initial statements for this form.
        """
        super().__init__()

        self.representations: mth.MonolingualTextHolder
        if isinstance(representations, mth.MonolingualTextHolder):
            self.representations = representations
        else:
            self.representations = mth.MonolingualTextHolder(representations)

        if isinstance(statements, sth.StatementHolder):
            self.statements = statements
        else:
            self.statements = sth.StatementHolder(statements)

        self.features: Set[i.Qid]
        if features is None:
            self.features = set()
        else:
            self.features = set(features)

        self.id: Optional[str] = None
        self.toremove: bool = False

    def get_published_settings(self) -> i.LexemeFormPublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the LexemeForm JSON dictionary which are only significant at editing time for existing lexeme forms.
        """
        if self.id is not None:
            return {
                "id": self.id,
            }
        return {}

    def set_published_settings(self, form_in: i.LexemeFormPublishedSettings) -> None:
        """Sets based on a LexemeForm JSON dictionary those variables which are only significant at editing time for existing lexeme forms.

        Args:
            form_in: Source statement of relevant editing information.
        """
        if "id" in form_in:
            self.id = form_in["id"]

    @overload
    def getitem(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...

    def getitem(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Retrieves a portion of a lexeme form.

        Args:
            arg: Indicator of part of a lexeme form.

        Returns:
            A list of statements or a form representation.

        Raises:
            KeyError: if the key provided can't be reasonably understood as referring to a part of a lexeme form.
        """
        if isinstance(arg, str):
            return self.statements[arg]
        elif isinstance(arg, tfsl.itemvalue.ItemValue):
            return self.statements[arg.id]
        elif isinstance(arg, (tfsl.languages.Language, tfsl.monolingualtext.MonolingualText)):
            return self.representations[arg]
        raise KeyError(f"Can't get {type(arg)} from LexemeForm")

    def __getitem__(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Gets statements or a form representation from this LexemeForm.

        Args:
            arg: Indicator of something to get from a LexemeForm.

        Returns:
            Either a list of statements or a form representation.
        """
        return self.getitem(arg)

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the LexemeForm.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the LexemeForm.
        """
        return self.statements.haswbstatement(property_in, value_in)

    def __add__(self, arg: object) -> "LexemeForm":
        """Adds an object to this LexemeForm.

        Args:
            arg: Object to add.

        Returns:
            LexemeForm reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be added to the LexemeForm.
        """
        if isinstance(arg, tfsl.monolingualtext.MonolingualText):
            published_settings = self.get_published_settings()
            form_out = LexemeForm(self.representations + arg, self.features, self.statements)
            form_out.set_published_settings(published_settings)
            return form_out
        elif isinstance(arg, str):
            published_settings = self.get_published_settings()
            if i.is_qid(arg):
                form_out = LexemeForm(self.representations, self.features | {arg}, self.statements)
            form_out.set_published_settings(published_settings)
            return form_out
        elif isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            form_out = LexemeForm(self.representations, self.features, self.statements + arg)
            form_out.set_published_settings(published_settings)
            return form_out
        raise NotImplementedError(f"Can't add {type(arg)} to LexemeForm")

    def __sub__(self, arg: object) -> "LexemeForm":
        """Removes an object from this LexemeForm.

        Args:
            arg: Object to remove.

        Returns:
            LexemeForm reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from a LexemeForm.
        """
        if isinstance(arg, (tfsl.languages.Language, tfsl.monolingualtext.MonolingualText)):
            published_settings = self.get_published_settings()
            form_out = LexemeForm(self.representations - arg, self.features, self.statements)
            form_out.set_published_settings(published_settings)
            return form_out
        elif isinstance(arg, str):
            published_settings = self.get_published_settings()
            if i.is_qid(arg):
                form_out = LexemeForm(self.representations, self.features - {arg}, self.statements)
            elif i.is_pid(arg):
                form_out = LexemeForm(self.representations, self.features, self.statements - arg)
            form_out.set_published_settings(published_settings)
            return form_out
        elif isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            form_out = LexemeForm(self.representations, self.features, self.statements - arg)
            form_out.set_published_settings(published_settings)
            return form_out
        raise NotImplementedError(f"Can't subtract {type(arg)} from LexemeForm")

    def set_to_remove(self) -> "LexemeForm":
        """Add an indication that this lexeme form should be removed when a change is published to Wikidata.

        Returns:
            Lexeme form with that indication.
        """
        published_settings = self.get_published_settings()
        form_out = LexemeForm(self.representations, self.features, self.statements)
        form_out.toremove = True
        form_out.set_published_settings(published_settings)
        return form_out

    def set_to_keep(self) -> "LexemeForm":
        """Remove any indication that this lexeme form should be removed when a change is published to Wikidata.

        Returns:
            Lexeme form with that indication removed.
        """
        published_settings = self.get_published_settings()
        form_out = LexemeForm(self.representations, self.features, self.statements)
        form_out.toremove = False
        form_out.set_published_settings(published_settings)
        return form_out

    def __contains__(self, arg: object) -> bool:
        """Checks for something inside the LexemeForm.

        Args:
            arg: Object to check for.

        Returns:
            Whether the object is inside the LexemeForm.
        """
        return self.contains(arg)

    @singledispatchmethod
    def contains(self, arg: object) -> bool:
        """Dispatches __contains__.

        Args:
            arg: Something to check for.

        Returns:
            Whether that object is in the LexemeForm.

        Raises:
            KeyError: if the argument can't be checked for in a LexemeForm.
        """
        raise KeyError(f"Can't check for {type(arg)} in LexemeForm")

    @contains.register(tfsl.languages.Language)
    @contains.register(tfsl.monolingualtext.MonolingualText)
    def _(self, arg: i.LanguageOrMT) -> bool:
        """Checks for a language in the form's representations.

        Args:
            arg: Language that (or MonolingualText whose language) will be checked for.

        Returns:
            Whether the form has a representation in that language.
        """
        return arg in self.representations

    @contains.register
    def _(self, arg: tfsl.claim.Claim) -> bool:
        """Checks for the property of a Claim in the form's statements.

        Args:
            arg: Claim whose property will be checked for.

        Returns:
            Whether the form has statements with that property.
        """
        return arg in self.statements

    @contains.register
    def _(self, arg: str) -> bool:
        """Checks for a property in the form's statements or a feature in the form's features.

        Args:
            arg: Either a property ID or an item ID.

        Returns:
            Whether the form has statements with the property, or has the provided feature in its feature set.

        Raises:
            TypeError: if the argument cannot be checked for in the lexeme form.
        """
        try:
            return arg in self.statements
        except TypeError:
            if i.is_qid(arg):
                return arg in self.features
            raise TypeError

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two lexeme forms.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this lexeme form is equal to the right-hand side.
        """
        if not isinstance(rhs, LexemeForm):
            return NotImplemented
        representations_equal = self.representations == rhs.representations
        features_equal = self.features == rhs.features
        statements_equal = self.statements == rhs.statements
        return representations_equal and features_equal and statements_equal

    def __str__(self) -> str:
        """Produces a representation of a lexeme form intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a LexemeForm.
        """
        base_str = str(self.representations)
        feat_str = ": " + ", ".join(self.features)
        stmt_str = str(self.statements)
        return "\n".join([base_str + feat_str, stmt_str])

    def __jsonout__(self) -> i.LexemeFormDict:
        """Produces a JSON serialization of this LexemeForm.

        Returns:
            JSON serialization of this LexemeForm.
        """
        reps_dict = self.representations.__jsonout__()
        base_dict: i.LexemeFormDict = {"representations": reps_dict, "grammaticalFeatures": list(self.features)}

        if self.id is not None:
            base_dict["id"] = self.id
        else:
            base_dict["add"] = ""
        if self.toremove:
            base_dict["remove"] = ""
            return base_dict

        if (statement_dict := self.statements.__jsonout__()) is not None:
            base_dict["claims"] = statement_dict
        else:
            base_dict["claims"] = {}

        return base_dict

    def to_editable(self) -> "LexemeForm":
        """No-op, since a LexemeForm is already editable.

        Returns:
            Itself.
        """
        return self


def build_form(form_in: i.LexemeFormDict) -> LexemeForm:
    """Builds a LexemeForm from the JSON dictionary describing it.

    Args:
        form_in: JSON representation of a LexemeForm.

    Returns:
        New LexemeForm object.
    """
    reps = mth.build_text_list(form_in["representations"])
    feats = form_in["grammaticalFeatures"]
    claims = sth.build_statement_list(form_in["claims"])

    form_out = LexemeForm(reps, feats, claims)
    form_out.set_published_settings(form_in)

    return form_out


class LF:
    """A LexemeForm, but form representations are not auto-converted to MonolingualTexts and statements are only assembled into Statements when accessed.

    See the documentation of LexemeFormLike methods for general information about
    what certain methods do.

    Attributes:
        features: Grammatical features of this form.
        representations: Representations of this form.
        id: Identifier for this form.
    """

    def __init__(self, form_json: i.LexemeFormDict) -> None:
        """Sets up an LF.

        Args:
            form_json: JSON for this lexeme form.
        """
        self.json = form_json

    def to_editable(self) -> LexemeForm:
        """Builds an editable LexemeForm object.

        Returns:
            LexemeForm based on the JSON content of this LF.
        """
        return build_form(self.json)

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the LexemeForm.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the LexemeForm.
        """
        return sth.haswbstatement(self.json["claims"], property_in, value_in)

    @property
    def features(self) -> List[i.Qid]:
        """(See LexemeFormLike.features for what this method does.)"""
        return self.json["grammaticalFeatures"]

    @property
    def representations(self) -> mth.MonolingualTextHolder:
        """(See LexemeFormLike.representations for what this method does.)"""
        return mth.MonolingualTextHolder(mth.build_text_list(self.json["representations"]))

    @property
    def id(self) -> str:
        """(See LexemeFormLike.id for what this method does.)"""
        return self.json["id"]

    def __repr__(self) -> str:
        """Produces string representation of the LexemeForm.

        Returns:
            String representation of the LexemeForm.
        """
        replen = len(self.json["representations"])
        featlen = len(self.json["grammaticalFeatures"])
        stmtlen = sum(len(y) for x, y in self.json["claims"].items())
        return f"<{self.id}: {replen} reps, {featlen} features, {stmtlen} statements>"

    @overload
    def getitem(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...

    def getitem(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Checks for something within a LexemeForm.

        Args:
            arg: Indicator of a part of a LexemeForm.

        Returns:
            A list of statements or a form representation.

        Raises:
            KeyError: if the argument can't be checked for in a LexemeForm.
        """
        if isinstance(arg, str):
            return self.getitem_str(arg)
        elif isinstance(arg, tfsl.itemvalue.ItemValue):
            return self.getitem_str(arg.id)
        elif isinstance(arg, tfsl.languages.Language):
            return mth.get_lang_from_mtlist(self.json["representations"], arg)
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            lang = arg.language
            lang_code = lang.code
            return tfsl.monolingualtext.build_lemma(self.json["representations"][lang_code])
        raise KeyError(f"Can't get {type(arg)} from LexemeSense")

    def __getitem__(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Gets statements or a form representation from this LexemeForm.

        Args:
            arg: Indicator of something to get from a LexemeForm.

        Returns:
            Either a list of statements or a form representation.
        """
        return self.getitem(arg)

    def getitem_str(self, key: str) -> i.StatementList:
        """Overload for getitem when the key is a string.

        Args:
            key: Indicator of a part of a lexeme form.

        Returns:
            A list of statements.

        Raises:
            KeyError: if the key is not a Pid.
        """
        if i.is_pid(key):
            return self.getitem_pid(key)
        raise KeyError

    def getitem_pid(self, key: i.Pid) -> i.StatementList:
        """Overload for getitem when the key is a property ID.

        Args:
            key: Indicator of a part of a lexeme form.

        Returns:
            A list of statements.
        """
        return self.get_stmts(key)

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the item with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return [tfsl.statement.build_statement(stmt) for stmt in self.json["claims"].get(prop, [])]

    def __jsonout__(self) -> i.LexemeFormDict:
        """Produces a JSON serialization of this LF.

        Returns:
            JSON serialization of this LF.
        """
        return self.json

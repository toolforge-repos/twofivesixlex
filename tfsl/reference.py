"""Holds the Reference class and a function to build one given a JSON representation of it."""

from collections import Counter, defaultdict
from copy import deepcopy
from functools import singledispatchmethod
from textwrap import indent
from typing import Any, DefaultDict, List, Optional, Union

import tfsl.claim
import tfsl.interfaces as i
import tfsl.utils


class ClaimSet(DefaultDict[i.Pid, i.ClaimList]):
    """Representation of a set of Claims."""

    def __init__(self, *args: Any, **kwargs: i.ClaimList) -> None:
        """Sets up a ClaimSet.

        Args:
            args: Positional arguments to collections.defaultdict.
            kwargs: Keyword arguments to collections.defaultdict.
        """
        if args:
            super(ClaimSet, self).__init__(*args, **kwargs)
        else:
            super(ClaimSet, self).__init__(list, **kwargs)

    def add(self, arg: tfsl.claim.Claim) -> "ClaimSet":
        """Adds a claim to a ClaimSet.

        Args:
            arg: Claim to be added.

        Returns:
            ClaimSet reflecting arg's addition.
        """
        newclaimset = deepcopy(self)
        newclaimset[arg.property].append(arg)
        return newclaimset

    def sub(self, arg: tfsl.claim.Claim) -> "ClaimSet":
        """Removes a claim from a ClaimSet.

        Args:
            arg: Claim to be removed.

        Returns:
            ClaimSet reflecting arg's removal.
        """
        newclaimset = deepcopy(self)
        newclaimset[arg.property] = [claim for claim in newclaimset[arg.property] if claim != arg]
        if len(newclaimset[arg.property]) == 0:
            del newclaimset[arg.property]
        return newclaimset


class Reference:
    """Representation of a reference."""

    def __init__(self, *args: Union[tfsl.claim.Claim, ClaimSet]) -> None:
        """Sets up a Reference.

        Args:
            args: Claim or claims going into the reference.
        """
        self._claims = ClaimSet()
        for arg in args:
            if isinstance(arg, tfsl.claim.Claim):
                self._claims = self._claims.add(arg)
            else:
                for prop in arg:
                    for claim in arg[prop]:
                        self._claims = self._claims.add(claim)

        self.snaks_order: Optional[List[i.Pid]] = None
        self.hash: Optional[str] = None

    def __getitem__(self, property_in: i.Pid) -> i.ClaimList:
        """Gets claims with a property from this Reference.

        Args:
            property_in: Ideally a property ID.

        Returns:
            List of claims with that property ID.
        """
        return self._claims[property_in]

    def __delitem__(self, arg: Union[i.Pid, tfsl.claim.Claim]) -> None:
        """Removes a claim or set of claims from a Reference.

        Args:
            arg: Either a property ID or a specific Claim.

        Todo:
            Should this yield a new Reference?
        """
        if isinstance(arg, tfsl.claim.Claim):
            self._claims[arg.property] = [claim for claim in self._claims[arg.property] if claim.value != arg.value]
        else:
            del self._claims[arg]

    def __add__(self, arg: object) -> "Reference":
        """Adds an object to this Reference.

        Args:
            arg: Object to add.

        Returns:
            Reference reflecting the added object.
        """
        newclaims = self.add(arg)
        return Reference(newclaims)

    @singledispatchmethod
    def add(self, arg: object) -> ClaimSet:
        """Dispatches __add__.

        Args:
            arg: Something to add to a Reference.

        Returns:
            Reference with arg added.

        Raises:
            ValueError: if the argument cannot be added to a Reference.
        """
        raise ValueError(f"Cannot add {type(arg)} to Reference")

    @add.register
    def _(self, arg: tfsl.claim.Claim) -> ClaimSet:
        """Adds a claim to the reference's set of claims.

        Args:
            arg: Claim to add.

        Returns:
            Set of claims with the provided claim added.
        """
        return self._claims.add(arg)

    def __sub__(self, arg: object) -> "Reference":
        """Removes an object from this Reference.

        Args:
            arg: Object to remove.

        Returns:
            Reference reflecting the removed object.
        """
        newclaims = self.sub(arg)
        return Reference(newclaims)

    @singledispatchmethod
    def sub(self, arg: object) -> ClaimSet:
        """Dispatches __sub__.

        Args:
            arg: Something to remove from a Reference.

        Returns:
            Reference with arg removed.

        Raises:
            ValueError: if the argument cannot be removed from a Reference.
        """
        raise ValueError(f"Cannot subtract {type(arg)} from Reference")

    @sub.register
    def _(self, arg: tfsl.claim.Claim) -> ClaimSet:
        """Removes a claim from the reference's set of claims.

        Args:
            arg: Claim to remove.

        Returns:
            Set of claims with the provided claim removed.
        """
        return self._claims.sub(arg)

    def __contains__(self, arg: object) -> bool:
        """Checks for an object inside a Reference.

        Args:
            arg: Object to check for.

        Returns:
            Whether the object is inside the Reference.
        """
        return self.contains(arg)

    @singledispatchmethod
    def contains(self, arg: object) -> bool:
        """Dispatches __contains__.

        Args:
            arg: Something to check for.

        Returns:
            Whether that object is in the Reference.

        Raises:
            KeyError: if the argument cannot be checked for in a Reference.
        """
        raise KeyError(f"Can't check for {type(arg)} in Reference")

    @contains.register
    def _(self, arg: tfsl.claim.Claim) -> bool:
        """Checks for the property of a claim in the reference's claims.

        Args:
            arg: Claim whose property is to be checked for.

        Returns:
            Whether that property is in the reference's claims.
        """
        return arg in self._claims[arg.property]

    @contains.register
    def _(self, arg: str) -> bool:
        """Checks for a property in the reference's claims.

        Args:
            arg: Property to check for.

        Returns:
            Whether the property is in the reference's claims.
        """
        return arg in self._claims

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two references.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this reference is equal to the right-hand side.
        """
        if not isinstance(rhs, Reference):
            return NotImplemented
        return Counter(self._claims) == Counter(rhs._claims)

    def __hash__(self) -> int:
        """Produces a hash for this Reference.

        Returns:
            Hash for this Reference.
        """
        return hash((claim for k, v in self._claims.items() for claim in v))

    def __str__(self) -> str:
        """Produces a representation of a reference intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Reference.
        """
        return "[" + indent("\n".join([str(claim) for key in self._claims for claim in self._claims[key]]), tfsl.utils.DEFAULT_INDENT) + "]"

    def __jsonout__(self) -> i.ReferenceDict:
        """Produces a JSON serialization of this Reference.

        Returns:
            JSON serialization of this Reference.
        """
        snaks_order = list(self._claims.keys())
        base_dict: i.ReferenceDict = {
            "snaks-order": snaks_order,
        }
        snak_dict = defaultdict(list)
        for snak in snaks_order:
            for claim in self._claims[snak]:
                snak_dict[snak].append(claim.__jsonout__())
        base_dict["snaks"] = dict(snak_dict)
        if self.hash is not None:
            base_dict["hash"] = self.hash
        return base_dict


def build_ref(ref_in: i.ReferenceDict) -> Reference:
    """Builds a Reference from the JSON dictionary describing it.

    Args:
        ref_in: JSON representation of a Reference.

    Returns:
        New Reference object.
    """
    claim_dict: i.ClaimDictSet = ref_in["snaks"]
    ref_claims: i.ClaimList = []
    for prop in claim_dict:
        for claim in claim_dict[prop]:
            ref_claims.append(tfsl.claim.build_claim(claim))

    ref_out = Reference(*ref_claims)
    ref_out.snaks_order = ref_in["snaks-order"]
    ref_out.hash = ref_in["hash"]
    return ref_out

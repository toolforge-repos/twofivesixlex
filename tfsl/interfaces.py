"""Intended to make certain commonly used derived types which depend only on tfsl itself easier to use.

That this file imports no other (except for type checking purposes) is intentional.
"""

import re
from typing import (
    TYPE_CHECKING, DefaultDict, Dict, List, NewType, Optional,
    Protocol, Sequence, Tuple, TypedDict, Union, overload,
)

from typing_extensions import NotRequired, TypeGuard

if TYPE_CHECKING:
    import tfsl.claim
    import tfsl.coordinatevalue
    import tfsl.item
    import tfsl.itemvalue
    import tfsl.languages
    import tfsl.lexeme
    import tfsl.lexemeform
    import tfsl.lexemesense
    import tfsl.monolingualtext
    import tfsl.quantityvalue
    import tfsl.statement
    import tfsl.timevalue

Qid_regex = re.compile(r"^Q[1-9]\d*$")
Pid_regex = re.compile(r"^P[1-9]\d*$")
Lid_regex = re.compile(r"^L[1-9]\d*$")
Fid_regex = re.compile(r"^F[1-9]\d*$")
Sid_regex = re.compile(r"^S[1-9]\d*$")
LFid_regex = re.compile(r"^(L[1-9]\d*)-(F[1-9]\d*)$")
LSid_regex = re.compile(r"^(L[1-9]\d*)-(S[1-9]\d*)$")

LanguageCode = NewType("LanguageCode", str)

Qid = NewType("Qid", str)


def is_qid(arg: str) -> TypeGuard[Qid]:
    """Checks that a string is a Qid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of a Qid.
    """
    return Qid_regex.match(arg) is not None


def get_qid_string(qid: Union[int, str]) -> Qid:
    """Extracts a Qid from a possible item reference.

    Args:
        qid: Representation of an item ID.

    Returns:
        Qid for that ID.

    Raises:
        ValueError: if an integer less than 1 was provided, or if a Qid string was otherwise not provided.
    """
    if isinstance(qid, int):
        if qid > 0:
            return Qid("Q" + str(qid))
        raise ValueError("integer for Qid not greater than 0")
    elif is_qid(qid):
        return qid
    raise ValueError("integer or Q string not provided")


Pid = NewType("Pid", str)


def is_pid(arg: str) -> TypeGuard[Pid]:
    """Checks that a string is a Pid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of a Pid.
    """
    return Pid_regex.match(arg) is not None


def get_pid_string(pid: Union[int, str]) -> Pid:
    """Extracts a Pid from a possible property reference.

    Args:
        pid: Representation of a property ID.

    Returns:
        Pid for that ID.

    Raises:
        ValueError: if an integer less than 1 was provided, or if a Pid string was otherwise not provided.
    """
    if isinstance(pid, int):
        if pid > 0:
            return Pid("P" + str(pid))
        raise ValueError("integer for Pid not greater than 0")
    elif is_pid(pid):
        return pid
    raise ValueError("integer or P string not provided")


Lid = NewType("Lid", str)


def is_lid(arg: str) -> TypeGuard[Lid]:
    """Checks that a string is an Lid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an Lid.
    """
    return Lid_regex.match(arg) is not None


Fid = NewType("Fid", str)


def is_fid(arg: str) -> TypeGuard[Fid]:
    """Checks that a string is an Fid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an Fid.
    """
    return Fid_regex.match(arg) is not None


Sid = NewType("Sid", str)


def is_sid(arg: str) -> TypeGuard[Sid]:
    """Checks that a string is an Sid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an Sid.
    """
    return Sid_regex.match(arg) is not None


LFid = NewType("LFid", str)


def is_lfid(arg: str) -> TypeGuard[LFid]:
    """Checks that a string is an LFid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an LFid.
    """
    return LFid_regex.match(arg) is not None


def split_lfid(arg: LFid) -> Optional[Tuple[Lid, Fid]]:
    """Splits an LFid into the Lid part and the Fid part.

    Args:
        arg: LFid to split.

    Returns:
        The lexeme ID and the form sub-ID within the LFid.
    """
    if matched_parts := LFid_regex.match(arg):
        lid_part: Optional[str] = matched_parts.group(1)
        fid_part: Optional[str] = matched_parts.group(2)
        if lid_part is not None and fid_part is not None and is_lid(lid_part) and is_fid(fid_part):
            return lid_part, fid_part
    return None


LSid = NewType("LSid", str)


def is_lsid(arg: str) -> TypeGuard[LSid]:
    """Checks that a string is an LSid.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an LSid.
    """
    return LSid_regex.match(arg) is not None


def split_lsid(arg: LSid) -> Optional[Tuple[Lid, Sid]]:
    """Splits an LSid into the Lid part and the Sid part.

    Args:
        arg: LSid to split.

    Returns:
        The lexeme ID and the sense sub-ID within the LSid.
    """
    if matched_parts := LSid_regex.match(arg):
        lid_part: Optional[str] = matched_parts.group(1)
        sid_part: Optional[str] = matched_parts.group(2)
        if (lid_part is not None and sid_part is not None) and (is_lid(lid_part) and is_sid(sid_part)):
            return lid_part, sid_part
    return None


PossibleLexemeReference = Union[int, Lid, LFid, LSid]


def get_lid_string(ref: PossibleLexemeReference) -> Lid:
    """Extracts an Lid from a possible lexeme reference.

    Args:
        ref: Representation of a lexeme ID.

    Returns:
        Lid for that ID.

    Raises:
        ValueError: if an integer less than 1 was provided, or if a Lid string was otherwise not provided.
    """
    if isinstance(ref, int):
        if ref > 0:
            return Lid("L" + str(ref))
        raise ValueError("integer for Lid not greater than 0")
    elif is_lsid(ref):
        if lsid_split := split_lsid(ref):
            lid, _ = lsid_split
            return lid
    elif is_lfid(ref):
        if lfid_split := split_lfid(ref):
            lid, _ = lfid_split
            return lid
    elif is_lid(ref):
        return ref
    raise ValueError("integer or L string not provided")


EntityId = Union[Qid, Pid, Lid, LFid, LSid]


def is_entityid(arg: str) -> TypeGuard[EntityId]:
    """Checks that a string is an EntityId.

    Args:
        arg: Some string.

    Returns:
        Whether this has the form of an EntityId.
    """
    return is_qid(arg) or is_lid(arg) or is_lsid(arg) or is_lfid(arg) or is_pid(arg)


class MonolingualTextDict(TypedDict):
    """Representation of the Wikibase "monolingualtext" datatype.

    Attributes:
        text: Actual text in the monolingual text.
        language: Language code for the monolingual text.
    """
    text: str
    language: LanguageCode


class CoordinateValueDict(TypedDict):
    """Representation of the Wikibase "globecoordinate" datatype.

    Attributes:
        latitude: Latitude of this coordinate.
        longitude: Longitude of this coordinate.
        altitude: Altitude of this coordinate.
        precision: Precision of this coordinate.
        globe: URL to a Wikidata item for the globe used.
    """
    latitude: float
    longitude: float
    altitude: Optional[float]
    precision: float
    globe: str


class QuantityValueDict(TypedDict):
    """Representation of the Wikibase "quantity" datatype.

    Attributes:
        amount: Base quantity.
        unit: Unit for this quantity.
        upperBound: Upper bound for uncertain quantities.
        lowerBound: Lower bound for uncertain quantities.
    """
    amount: float
    unit: str
    upperBound: NotRequired[float]  # noqa: N815
    lowerBound: NotRequired[float]  # noqa: N815


class TimeValueDict(TypedDict):
    """Representation of the Wikibase "time" datatype.

    Attributes:
        time: Actual date/time string stored.
        timezone: (Currently unused.)
        before: (Currently unused.)
        after: (Currently unused.)
        precision: Indicator of how precise the date/time is.
        calendarmodel: URL to a Wikidata item for the calendar used.
    """
    time: str
    timezone: int
    before: int
    after: int
    precision: int
    calendarmodel: str


ItemValueDict = TypedDict("ItemValueDict", {"entity-type": str, "id": EntityId, "numeric-id": NotRequired[int]})

ClaimDictValueDictionary = Union[CoordinateValueDict, MonolingualTextDict, ItemValueDict, QuantityValueDict, TimeValueDict]
ClaimDictValue = Union[str, ClaimDictValueDictionary]


class ClaimDictDatavalue(TypedDict):
    """The actual value of a Claim or of a Statement.

    Attributes:
        value: Value of the claim.
        type: Type of that value.
    """
    value: ClaimDictValue
    type: str


class ClaimDict(TypedDict, total=False):
    """A property-value pairing in places other than the main portion of a statement, such as a qualifier or a reference.

    Attributes:
        property: ID for the property in this claim.
        snaktype: Either "value", "novalue", or "somevalue".
        hash: Hash of the data in the claim.
        datavalue: Serialized claim value.
        datatype: String indicating the type of the value.
    """
    property: Pid
    snaktype: str
    hash: str
    datavalue: ClaimDictDatavalue
    datatype: str


ClaimValue = Union[
    bool,
    "tfsl.coordinatevalue.CoordinateValue",
    "tfsl.itemvalue.ItemValue",
    "tfsl.monolingualtext.MonolingualText",
    "tfsl.quantityvalue.QuantityValue",
    str,
    "tfsl.timevalue.TimeValue",
]

ClaimList = List["tfsl.claim.Claim"]
StatementList = List["tfsl.statement.Statement"]
MonolingualTextList = List["tfsl.monolingualtext.MonolingualText"]

LexemeSenseLikeList = Sequence["tfsl.lexemesense.LexemeSenseLike"]
LexemeFormLikeList = Sequence["tfsl.lexemeform.LexemeFormLike"]
LexemeLikeList = List["tfsl.lexeme.LexemeLike"]

LexemeSenseList = List["tfsl.lexemesense.LexemeSense"]
LexemeFormList = List["tfsl.lexemeform.LexemeForm"]
LexemeList = List["tfsl.lexeme.Lexeme"]

ReferenceList = List["tfsl.reference.Reference"]

ClaimDictSet = Dict[Pid, List[ClaimDict]]

ClaimSet = Dict[Pid, List["tfsl.claim.Claim"]]


class LanguageDict(TypedDict):
    """JSON representation of a Language.

    Attributes:
        code: Language code.
        item: Item ID for the language.
    """
    code: str
    item: Qid


ReferenceDict = TypedDict(
    "ReferenceDict", {
        "snaks-order": List[Pid],
        "snaks": ClaimDictSet,
        "hash": NotRequired[str],
    }, total=False,
)

StatementDictPublishedSettings = TypedDict(
    "StatementDictPublishedSettings", {
        "id": NotRequired[str],
        "qualifiers-order": NotRequired[List[Pid]],
        "remove": NotRequired[str],
    }, total=False,
)


class StatementData(TypedDict, total=False):
    """Those entries in a StatementDict which pertain to informational content.

    Attributes:
        mainsnak: Actual claim made by the statement.
        type: Usually the word 'statement'.
        qualifiers: Serialized qualifier mapping.
        rank: String indicating the statement's rank.
        references: List of serialized references.
    """
    mainsnak: ClaimDict
    type: str
    qualifiers: NotRequired[ClaimDictSet]
    rank: str
    references: NotRequired[List[ReferenceDict]]


class StatementDict(StatementData, StatementDictPublishedSettings):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, the dictionaries in the arrays underneath the dictionary represented by the XPath "/entities/L301993/claims"."""


StatementDictSet = Dict[Pid, List[StatementDict]]

StatementSet = DefaultDict[Pid, List["tfsl.statement.Statement"]]


class LemmaDict(TypedDict, total=False):
    """Pairings of a language with a string value, such as, among others, in the output of wikidata.org/wiki/Special:EntityData/L301993.json, the dictionaries inside the dictionary represented by the XPath "/entities/L301993/lemmas".

    Attributes:
        language: Language code for the string value.
        value: The actual string value.
        remove: If present, an empty string indicating that the string value should be removed.
    """
    language: LanguageCode
    value: str
    remove: NotRequired[str]


LemmaDictSet = Dict[LanguageCode, LemmaDict]


class LexemeFormPublishedSettings(TypedDict, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993-F1.json, those entries in the dictionary represented by the XPath "/entities/L301993-F1" which are only relevant at editing time and not otherwise in EntityPublishedSettings.

    Attributes:
        id: ID for the form.
    """
    id: NotRequired[str]


class LexemeFormData(TypedDict, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993-F1.json, those entries in the dictionary represented by the XPath "/entities/L301993-F1" which pertain to informational content.

    Attributes:
        representations: Serialized representation mapping.
        grammaticalFeatures: List of item IDs for grammatical features.
        claims: Serialized statement set.
        add: If present, an empty string indicating that this is a new form.
    """
    representations: LemmaDictSet
    grammaticalFeatures: List[Qid]  # noqa: N815
    claims: StatementDictSet
    add: NotRequired[str]


class LexemeFormDict(LexemeFormPublishedSettings, LexemeFormData):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, the entries in the array represented by the XPath "/entities/L301993/forms".

    Alternatively, in the output of wikidata.org/wiki/Special:EntityData/L301993-F1.json, the dictionary represented by the XPath "/entities/L301993-F1".
    """


class LexemeSensePublishedSettings(TypedDict, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993-S1.json, those entries in the dictionary represented by the XPath "/entities/L301993-S1" which are only relevant at editing time and not otherwise in EntityPublishedSettings.

    Attributes:
        id: ID for the sense.
    """
    id: NotRequired[str]


class LexemeSenseData(TypedDict, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993-S1.json, those entries in the dictionary represented by the XPath "/entities/L301993-S1" which pertain to informational content.

    Attributes:
        glosses: Serialized gloss mapping.
        claims: Serialized statement set.
        add: If present, an empty string indicating that this is a new sense.
    """
    glosses: LemmaDictSet
    claims: StatementDictSet
    add: NotRequired[str]


class LexemeSenseDict(LexemeSensePublishedSettings, LexemeSenseData):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, the entries in the array represented by the XPath "/entities/L301993/senses".

    Alternatively, in the output of wikidata.org/wiki/Special:EntityData/L301993-S1.json, the dictionary represented by the XPath "/entities/L301993-S1".
    """


class EntityPublishedSettings(TypedDict, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/Q1356.json, those entries in the dictionary represented by the XPath "/entities/Q1356" which are only relevant at editing time and are common to all entities returned by Special:EntityData.

    Attributes:
        pageid: Integral ID of the page for the entity.
        ns: Integral ID of the namespace.
        title: Name of the page for the entity.
        lastrevid: Integral ID of the last revision to the entity.
        modified: Timestamp of the last modification to the entity.
    """
    pageid: NotRequired[int]
    ns: NotRequired[int]
    title: NotRequired[str]
    lastrevid: NotRequired[int]
    modified: NotRequired[str]


class LexemeData(TypedDict):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, those entries in the dictionary represented by the XPath "/entities/L301993" which pertain to informational content.

    Attributes:
        lexicalCategory: Item ID for the lexical category.
        language: Item ID for the language.
        lemmas: Serialized lemma mapping.
        claims: Serialized statement set.
        forms: Serialized list of forms.
        senses: Serialized list of senses.
    """
    lexicalCategory: Qid  # noqa: N815
    language: Qid
    lemmas: LemmaDictSet
    claims: StatementDictSet
    forms: List[LexemeFormDict]
    senses: List[LexemeSenseDict]


class LexemePublishedSettings(EntityPublishedSettings, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, those entries in the dictionary represented by the XPath "/entities/L301993" which are only relevant at editing time and not otherwise in EntityPublishedSettings.

    Attributes:
        id: Item ID.
        type: Usually the string "lexeme".
    """
    id: NotRequired[Lid]
    type: NotRequired[str]


class LexemeDict(LexemePublishedSettings, LexemeData):
    """In the output of wikidata.org/wiki/Special:EntityData/L301993.json, the dictionary represented by the XPath "/entities/L301993"."""


class SitelinkDict(TypedDict):
    """In the output of wikidata.org/wiki/Special:EntityData/Q1356.json, the dictionaries inside the dictionary represented by the XPath "/entities/Q1356/sitelinks".

    Attributes:
        site: Site on which the page resides.
        title: Name of the page on that site.
        badges: List of item IDs representing applied badges.
        url: URL to the page in question.
    """
    site: str
    title: str
    badges: List[Qid]
    url: str


class PropertyData(TypedDict):
    """In the output of wikidata.org/wiki/Special:EntityData/P5578.json, those entries in the dictionary represented by the XPath "/entities/P5578" which pertain to informational content.

    Attributes:
        datatype: Indicator of the value datatype of a claim with this property.
        labels: Serialized label mapping.
        descriptions: Serialized description mapping.
        aliases: Serialized alias mapping.
        claims: Serialized statement set.
    """
    datatype: str
    labels: LemmaDictSet
    descriptions: LemmaDictSet
    aliases: Dict[LanguageCode, List[LemmaDict]]
    claims: StatementDictSet


class ItemData(TypedDict):
    """In the output of wikidata.org/wiki/Special:EntityData/Q1356.json, those entries in the dictionary represented by the XPath "/entities/Q1356" which pertain to informational content.

    Attributes:
        labels: Serialized label mapping.
        descriptions: Serialized description mapping.
        aliases: Serialized alias mapping.
        claims: Serialized statement set.
        sitelinks: Serialized sitelink mapping.
    """
    labels: LemmaDictSet
    descriptions: LemmaDictSet
    aliases: Dict[LanguageCode, List[LemmaDict]]
    claims: StatementDictSet
    sitelinks: Dict[str, SitelinkDict]


class ItemPublishedSettings(EntityPublishedSettings, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/Q1356.json, those entries in the dictionary represented by the XPath "/entities/Q1356" which are only relevant at editing time and not otherwise in EntityPublishedSettings.

    Attributes:
        id: Item ID.
        type: Usually the string "item".
    """
    id: NotRequired[Qid]
    type: NotRequired[str]


class PropertyPublishedSettings(EntityPublishedSettings, total=False):
    """In the output of wikidata.org/wiki/Special:EntityData/P5578.json, those entries in the dictionary represented by the XPath "/entities/P5578" which are only relevant at editing time and not otherwise in EntityPublishedSettings.

    Attributes:
        id: Property ID.
        type: Usually the string "property".
    """
    id: NotRequired[Pid]
    type: NotRequired[str]


class PropertyDict(PropertyPublishedSettings, PropertyData):
    """In the output of wikidata.org/wiki/Special:EntityData/P5578.json, the dictionary represented by the XPath "/entities/P5578"."""


class ItemDict(ItemPublishedSettings, ItemData):
    """In the output of wikidata.org/wiki/Special:EntityData/Q1356.json, the dictionary represented by the XPath "/entities/Q1356"."""


class MTST(Protocol):
    """A number of Wikibase entities contain as top-level subentities 1) a container of statements and 2) a container of monolingual text values. This Protocol exists to be used by other Protocols to allow accesses of these entities in similar ways."""

    def haswbstatement(self, property_in: Pid, value_in: Optional[ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) in the MTST.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists in the MTST.
        """

    @overload
    def getitem(self, arg: "tfsl.languages.Language") -> "tfsl.monolingualtext.MonolingualText": ...
    @overload
    def getitem(self, arg: "tfsl.monolingualtext.MonolingualText") -> "tfsl.monolingualtext.MonolingualText": ...
    @overload
    def getitem(self, arg: Pid) -> StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> StatementList: ...


def is_entitypublishedsettings(arg: TypedDict) -> TypeGuard[EntityPublishedSettings]:
    """Checks that the keys expected for a published entity exist.

    Args:
        arg: Some dictionary.

    Returns:
        Whether this has the keys of an entity serialization.
    """
    return all(x in arg for x in ["pageid", "ns", "title", "lastrevid", "modified"])


def is_propertydict(arg: EntityPublishedSettings) -> TypeGuard[PropertyDict]:
    """Checks that the keys expected for a Property exist.

    Args:
        arg: Serialization of some entity.

    Returns:
        Whether this has the keys of a Property serialization.
    """
    return all(x in arg for x in ["datatype", "labels", "descriptions", "aliases", "claims"])


def is_itemdict(arg: EntityPublishedSettings) -> TypeGuard[ItemDict]:
    """Checks that the keys expected for an Item exist.

    Args:
        arg: Serialization of some entity.

    Returns:
        Whether this has the keys of an Item serialization.
    """
    return all(x in arg for x in ["labels", "descriptions", "aliases", "claims", "sitelinks"])


def is_lexemedict(arg: EntityPublishedSettings) -> TypeGuard[LexemeDict]:
    """Checks that the keys expected for a Lexeme exist.

    Args:
        arg: Serialization of some entity.

    Returns:
        Whether this has the keys of a Lexeme serialization.
    """
    return all(x in arg for x in ["lemmas", "lexicalCategory", "language", "claims", "forms", "senses"])


def is_lexemeformdict(arg: EntityPublishedSettings) -> TypeGuard[LexemeFormDict]:
    """Checks that the keys expected for a LexemeForm exist.

    Args:
        arg: Serialization of some entity.

    Returns:
        Whether this has the keys of a LexemeForm serialization.
    """
    return all(x in arg for x in ["representations", "grammaticalFeatures", "claims"])


def is_lexemesensedict(arg: EntityPublishedSettings) -> TypeGuard[LexemeSenseDict]:
    """Checks that the keys expected for a LexemeSense exist.

    Args:
        arg: Serialization of some entity.

    Returns:
        Whether this has the keys of a LexemeSense serialization.
    """
    return all(x in arg for x in ["glosses", "claims"])


Entity = Union[
    "tfsl.lexeme.Lexeme",
    "tfsl.lexemeform.LexemeForm",
    "tfsl.lexemesense.LexemeSense",
]
EntityLike = Union[
    "tfsl.lexemeform.LexemeFormLike",
    "tfsl.lexemesense.LexemeSenseLike",
]
EntityDict = Union[LexemeDict, LexemeFormDict, LexemeSenseDict]

StatementHolderInput = Union[StatementSet, StatementList]

MonolingualTextHolderInput = Union["tfsl.monolingualtext.MonolingualText", MonolingualTextList]

LanguageOrMT = Union["tfsl.languages.Language", "tfsl.monolingualtext.MonolingualText"]

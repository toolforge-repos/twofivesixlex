"""Holds the MonolingualText class and a function to build one given a JSON representation of it."""

from typing import TYPE_CHECKING

import tfsl.interfaces as i
import tfsl.languages

if TYPE_CHECKING:
    from typing_extensions import TypeGuard


class MonolingualText:
    """Representation of a value to which a language is tied.

    As far as claims go, this is usable directly as a monolingual text value;
    it can, however, be used to specify a language with accompanying text,
    such as is useful to determine terms in a termbox or lexeme representations.
    """

    def __init__(self, text: str, language: "tfsl.languages.Language") -> None:
        """Sets up a MonolingualText.

        Args:
            text: Text of this MonolingualText.
            language: Language of this MonolingualText.
        """
        self.text: str = text
        self.language: "tfsl.languages.Language" = language

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two monolingual texts.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this monolingual text is equal to the right-hand side.
        """
        if not isinstance(rhs, MonolingualText):
            return NotImplemented
        return self.text == rhs.text and self.language == rhs.language

    def __hash__(self) -> int:
        """Produces a hash for this MonolingualText.

        Returns:
            Hash for this MonolingualText.
        """
        return hash((self.text, self.language))

    def __str__(self) -> str:
        """Produces a representation of a monolingual text intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a MonolingualText.
        """
        return f"{self.text}@{self.language.code} ({self.language.item})"

    def __repr__(self) -> str:
        """Produces string representation of the MonolingualText.

        Returns:
            String representation of the MonolingualText.
        """
        return f"{self.text}@{self.language.code} ({self.language.item})"

    def __jsonout__(self) -> i.MonolingualTextDict:
        """Produces a JSON serialization of this MonolingualText.

        Returns:
            JSON serialization of this MonolingualText.
        """
        return {
            "text": self.text,
            "language": self.language.code,
        }


def is_mtvalue(value_in: i.ClaimDictValueDictionary) -> "TypeGuard[i.MonolingualTextDict]":
    """Checks that the keys expected for a MonolingualText exist.

    Args:
        value_in: JSON serialization of some claim value.

    Returns:
        Whether this has the keys of a MonolingualText serialization.
    """
    return all(key in value_in for key in ["text", "language"])


def build_mtvalue(value_in: i.MonolingualTextDict) -> MonolingualText:
    """Builds a MonolingualText given the Wikibase JSON for one.

    Args:
        value_in: JSON representation of a MonolingualText.

    Returns:
        New MonolingualText object.
    """
    return MonolingualText(value_in["text"], tfsl.languages.get_first_lang(value_in["language"]))


def build_lemma(value_in: i.LemmaDict) -> MonolingualText:
    """Builds a MonolingualText given the Wikibase JSON for a label, lemma, or form representation.

    Args:
        value_in: JSON representation of a MonolingualText.

    Returns:
        New MonolingualText object.
    """
    return MonolingualText(value_in["value"], tfsl.languages.get_first_lang(value_in["language"]))

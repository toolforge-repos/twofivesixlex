"""Holds the Statement class and a function to build one given a JSON representation of it."""

from collections import defaultdict
from copy import deepcopy
from enum import Enum
from textwrap import indent
from typing import List, Optional, Union

import tfsl.claim
import tfsl.interfaces as i
import tfsl.reference
import tfsl.utils


class Rank(Enum):
    """Represents the rank of a given statement.

    Attributes:
        Preferred: Indicating the most suitable value among possibilities for any number of reasons.
        Normal: Indicating a value that was true in the past (whether or not it is true now) or is technically correct but not suitable enough for any number of reasons.
        Deprecated: Indicating a value that was never true.
    """
    Preferred = 1
    Normal = 0
    Deprecated = -1


class Statement:
    """Represents a statement, or a claim with accompanying rank, optional qualifiers, and optional references."""

    def __init__(
        self,
        property_in: i.Pid,
        value_in: i.ClaimValue,
        rank: Optional[Rank] = None,
        qualifiers: Optional[Union[i.ClaimList, tfsl.reference.ClaimSet]] = None,
        references: Optional[i.ReferenceList] = None,
    ) -> None:
        """Sets up a Statement.

        Args:
            property_in: Property ID for this statement.
            value_in: Value of this statement.
            rank: Rank of this statement.
            qualifiers: Qualifiers to be applied to the statement.
            references: References to be applied to the statement.

        Raises:
            TypeError: if the type of the value does not match the type expected by the property.
        """
        self.rank: Rank
        if rank is None:
            self.rank = Rank.Normal
        else:
            self.rank = rank

        self.property: i.Pid = property_in
        self.value: i.ClaimValue
        if tfsl.utils.is_novalue(value_in) or tfsl.utils.is_somevalue(value_in):
            self.value = value_in
        else:
            value_type = type(value_in)
            property_type = tfsl.claim.type_string_to_type[tfsl.utils.values_type(self.property)]
            if property_type == value_type:
                self.value = value_in
            else:
                raise TypeError(f"Providing {value_type} as {self.property} value where {property_type} expected")

        self.qualifiers: tfsl.reference.ClaimSet = tfsl.reference.ClaimSet()

        if isinstance(qualifiers, tfsl.reference.ClaimSet):
            for prop in qualifiers:
                for claim in qualifiers[prop]:
                    self.qualifiers = self.qualifiers.add(claim)
        elif qualifiers is not None:
            for arg in qualifiers:
                self.qualifiers = self.qualifiers.add(arg)

        self.references: i.ReferenceList
        if references is None:
            self.references = []
        else:
            self.references = deepcopy(references)

        self.id: Optional[str] = None
        self.qualifiers_order: List[i.Pid] = []
        self.toremove: bool = False

    def __getitem__(self, key: str) -> i.ClaimList:
        """Gets qualifiers with a property from this Statement.

        Args:
            key: Ideally a property ID.

        Returns:
            List of qualifiers with that property ID.

        Raises:
            KeyError: if arg is not a property ID.
        """
        if i.is_pid(key):
            return self.qualifiers.get(key, [])
        raise KeyError

    def __add__(self, arg: object) -> "Statement":
        """Adds an object to this Statement.

        Args:
            arg: Object to add.

        Returns:
            Statement reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be added to the Statement.
        """
        if isinstance(arg, tfsl.claim.Claim):
            return self.add_claim(arg)
        elif isinstance(arg, tfsl.reference.Reference):
            return self.add_reference(arg)
        raise NotImplementedError(f"Can't add {str(type(arg))} to statement")

    def __sub__(self, arg: object) -> "Statement":
        """Removes an object from this Statement.

        Args:
            arg: Object to remove.

        Returns:
            Statement reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from a Statement.
        """
        if isinstance(arg, tfsl.claim.Claim):
            return self.sub_claim(arg)
        elif isinstance(arg, tfsl.reference.Reference):
            return self.sub_reference(arg)
        raise NotImplementedError(f"Can't subtract {str(type(arg))} from statement")

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a qualifier property (with an optional value) on the statement.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Qualifier property to check for.
            value_in: Qualifier value to check for.

        Returns:
            Whether the property (with the value provided) is used as a qualifier on the statement.
        """
        if value_in is None:
            return property_in in self.qualifiers
        elif tfsl.utils.is_novalue(value_in):
            def compare_function(stmt: tfsl.claim.Claim) -> bool:
                """Compares a qualifier's value to novalue.

                Args:
                    stmt: Qualifier to compare against.

                Returns:
                    Whether stmt's value is novalue.
                """
                return tfsl.utils.is_novalue(stmt.value)
        elif tfsl.utils.is_somevalue(value_in):
            def compare_function(stmt: tfsl.claim.Claim) -> bool:
                """Compares a qualifier's value to somevalue.

                Args:
                    stmt: Qualifier to compare against.

                Returns:
                    Whether stmt's value is somevalue.
                """
                return tfsl.utils.is_somevalue(stmt.value)
        else:
            def compare_function(stmt: tfsl.claim.Claim) -> bool:
                """Compares a qualifier's value to another value.

                Args:
                    stmt: Qualifier to compare against.

                Returns:
                    Whether stmt's value and the other value are equal.
                """
                return stmt.value == value_in
        return any(map(compare_function, self.qualifiers[property_in]))

    def add_claim(self, arg: tfsl.claim.Claim) -> "Statement":
        """Adds a qualifier to a Statement.

        Args:
            arg: Qualifier to add.

        Returns:
            Statement with the qualifier added.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers.add(arg), self.references)
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def add_reference(self, arg: tfsl.reference.Reference) -> "Statement":
        """Adds a Reference to a Statement.

        Args:
            arg: Reference to add.

        Returns:
            Statement with the reference added.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers, tfsl.utils.add_to_list(self.references, arg))
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def sub_claim(self, arg: tfsl.claim.Claim) -> "Statement":
        """Removes a qualifier from a Statement.

        Args:
            arg: Qualifier to remove.

        Returns:
            Statement with the qualifier removed.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers.sub(arg), self.references)
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def sub_reference(self, arg: tfsl.reference.Reference) -> "Statement":
        """Removes a Reference from a Statement.

        Args:
            arg: Reference to remove.

        Returns:
            Statement with the reference removed.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers, tfsl.utils.sub_from_list(self.references, arg))
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def __matmul__(self, arg: object) -> "Statement":
        """Sets the rank of the Statement.

        Args:
            arg: Ideally a Rank.

        Returns:
            Statement reflecting the modified Rank.

        Raises:
            NotImplementedError: if the object cannot be used to set a rank on the Statement.
        """
        if isinstance(arg, Rank):
            if arg == self.rank:
                return self
            return Statement(self.property, self.value, arg, self.qualifiers, self.references)
        raise NotImplementedError(f"{str(type(arg))} is not a rank")

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two statements.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this statement is equal to the right-hand side.
        """
        if isinstance(rhs, tfsl.claim.Claim):
            return self.property == rhs.property and self.value == rhs.value
        elif isinstance(rhs, Statement):
            return self.property == rhs.property and self.value == rhs.value and self.rank == rhs.rank and self.qualifiers == rhs.qualifiers and self.references == rhs.references
        return NotImplemented

    def set_to_remove(self) -> "Statement":
        """Add an indication that this statement should be removed when a change is published to Wikidata.

        Returns:
            Statement with that indication.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers, self.references)
        stmt_out.toremove = True
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def set_to_keep(self) -> "Statement":
        """Remove any indication that this statement should be removed when a change is published to Wikidata.

        Returns:
            Statement with that indication removed.
        """
        published_settings = self.get_published_settings()
        stmt_out = Statement(self.property, self.value, self.rank, self.qualifiers, self.references)
        stmt_out.toremove = False
        stmt_out.set_published_settings(published_settings)
        return stmt_out

    def set_published_settings(self, stmt_in: i.StatementDictPublishedSettings) -> None:
        """Sets based on a Statement JSON dictionary those variables which are only significant at editing time for existing statements.

        Args:
            stmt_in: Source statement of relevant editing information.
        """
        if "id" in stmt_in:
            self.id = stmt_in["id"]
            self.qualifiers_order = stmt_in.get("qualifiers-order", [])

    def get_published_settings(self) -> i.StatementDictPublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the Statement JSON dictionary which are only significant at editing time for existing statements.
        """
        if self.id is not None:
            return {
                "id": self.id,
                "qualifiers-order": self.qualifiers_order,
            }
        return {}

    def __str__(self) -> str:
        """Produces a representation of a statement intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Statement.
        """
        base_str = f"{self.property}: {self.value} ({self.rank})"
        qualifiers_str = ""
        references_str = ""

        if self.qualifiers != {}:
            qualifiers_str = "(\n" + indent("\n".join([str(qual) for key in self.qualifiers for qual in self.qualifiers[key]]), tfsl.utils.DEFAULT_INDENT) + "\n)"
        if len(self.references) != 0:
            references_str = "[\n" + indent("\n".join([str(ref) for ref in self.references]), tfsl.utils.DEFAULT_INDENT) + "\n]"
        return base_str + qualifiers_str + references_str

    def __jsonout__(self) -> i.StatementDict:
        """Produces a JSON serialization of this Statement.

        Returns:
            JSON serialization of this Statement.
        """
        base_dict: i.StatementDict = {"type": "statement", "mainsnak": tfsl.claim.Claim(self.property, self.value).__jsonout__()}
        if self.id is not None:
            base_dict["id"] = self.id
        if self.toremove:
            base_dict["remove"] = ""
            return base_dict

        base_dict["rank"] = ["deprecated", "normal", "preferred"][self.rank.value + 1]
        base_dict["qualifiers"] = defaultdict(list)
        for stmtprop, stmtval in self.qualifiers.items():
            base_dict["qualifiers"][stmtprop].extend([stmt.__jsonout__() for stmt in stmtval])
        if base_dict["qualifiers"] == {}:
            del base_dict["qualifiers"]
        else:
            base_dict["qualifiers"] = dict(base_dict["qualifiers"])
        base_dict["qualifiers-order"] = list(self.qualifiers.keys())
        if base_dict["qualifiers-order"] == []:
            del base_dict["qualifiers-order"]
        base_dict["references"] = [reference.__jsonout__() for reference in self.references]
        return base_dict

    def get_itemvalue(
        self,
        novalue: Optional[tfsl.itemvalue.ItemValue] = None,
        somevalue: Optional[tfsl.itemvalue.ItemValue] = None,
    ) -> tfsl.itemvalue.ItemValue:
        """Gets an ItemValue from the statement.

        Args:
            novalue: Value to return if the statement's value is novalue.
            somevalue: Value to return if the statement's value is somevalue.

        Returns:
            Claim's value if it is an ItemValue, or the 'somevalue' argument if the statement's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the statement did not yield an ItemValue.
        """
        if isinstance(self.value, tfsl.itemvalue.ItemValue):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield an ItemValue")

    def get_monolingualtext(
        self,
        novalue: Optional[tfsl.monolingualtext.MonolingualText] = None,
        somevalue: Optional[tfsl.monolingualtext.MonolingualText] = None,
    ) -> tfsl.monolingualtext.MonolingualText:
        """Gets a MonolingualText from the statement.

        Args:
            novalue: Value to return if the statement's value is novalue.
            somevalue: Value to return if the statement's value is somevalue.

        Returns:
            Claim's value if it is a MonolingualText, or the 'somevalue' argument if the statement's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the statement did not yield a MonolingualText.
        """
        if isinstance(self.value, tfsl.monolingualtext.MonolingualText):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield a MonolingualText")

    def get_str(
        self,
        novalue: Optional[str] = None,
        somevalue: Optional[str] = None,
    ) -> str:
        """Gets a string from the statement.

        Args:
            novalue: Value to return if the statement's value is novalue.
            somevalue: Value to return if the statement's value is somevalue.

        Returns:
            Claim's value if it is a string, or the 'somevalue' argument if the statement's value is somevalue, or likewise with respect to 'novalue', or fails entirely.

        Raises:
            TypeError: if the statement did not yield a string.
        """
        if isinstance(self.value, str):
            return self.value
        if self.value is True and somevalue is not None:
            return somevalue
        if self.value is False and novalue is not None:
            return novalue
        raise TypeError(f"{self.property} statement did not yield a string")


def build_quals(quals_in: Optional[i.ClaimDictSet] = None) -> tfsl.reference.ClaimSet:
    """Builds a set of qualifiers given a JSON dictionary representing it.

    Args:
        quals_in: JSON serialization of some qualifiers.

    Returns:
        New ClaimSet.
    """
    quals = tfsl.reference.ClaimSet()
    if quals_in is not None:
        for prop in quals_in:
            for qual in quals_in[prop]:
                quals[prop].append(tfsl.claim.build_claim(qual))
    return quals


def build_statement(stmt_in: i.StatementDict) -> Statement:
    """Builds a Statement from the JSON dictionary describing it.

    Args:
        stmt_in: JSON representation of a Statement.

    Returns:
        New Statement object.
    """
    stmt_rank = Rank.Normal
    if stmt_in["rank"] == "preferred":
        stmt_rank = Rank.Preferred
    elif stmt_in["rank"] == "deprecated":
        stmt_rank = Rank.Deprecated

    stmt_mainsnak = stmt_in["mainsnak"]
    stmt_property = stmt_mainsnak["property"]
    stmt_value: i.ClaimValue
    if stmt_mainsnak["snaktype"] == "novalue":
        stmt_value = False
    elif stmt_mainsnak["snaktype"] == "somevalue":
        stmt_value = True
    else:
        stmt_datavalue = stmt_mainsnak["datavalue"]
        stmt_value = tfsl.claim.build_value(stmt_datavalue["value"])
    stmt_quals = build_quals(stmt_in.get("qualifiers"))
    stmt_refs = []
    if stmt_in.get("references", False):
        stmt_refs = [tfsl.reference.build_ref(ref) for ref in stmt_in["references"]]

    stmt_out = Statement(stmt_property, stmt_value, stmt_rank, stmt_quals, stmt_refs)
    stmt_out.set_published_settings(stmt_in)
    return stmt_out

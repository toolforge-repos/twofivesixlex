"""Miscellaneous utility functions."""

from copy import deepcopy
from functools import lru_cache
from typing import Any, List, TypeVar

import tfsl.auth
import tfsl.interfaces as i

DEFAULT_INDENT = "    "
WD_PREFIX = "http://www.wikidata.org/entity/"


def prefix_wd(arg: str) -> str:
    """Adds the entity prefix to the provided string.

    Args:
        arg: String to modify.

    Returns:
        Arg with entity prefix added.
    """
    return WD_PREFIX + arg


def strip_prefix_wd(arg: str) -> str:
    """Removes the entity prefix from the provided string.

    Args:
        arg: String to modify.

    Returns:
        Arg with entity prefix removed.
    """
    if arg.startswith(WD_PREFIX):
        return arg[len(WD_PREFIX):]
    return arg


ListT = TypeVar("ListT")


def add_to_list(references: List[ListT], arg: ListT) -> List[ListT]:
    """Adds a ListT to a list of ListTs.

    Args:
        references: List of ListTs.
        arg: Item to add.

    Returns:
        List of ListTs with arg added.
    """
    newreferences = deepcopy(references)
    newreferences.append(arg)
    return newreferences


def sub_from_list(references: List[ListT], arg: ListT) -> List[ListT]:
    """Removes a ListT from a list of ListTs.

    Args:
        references: List of ListTs.
        arg: Item to remove.

    Returns:
        List of ListTs with arg removed.
    """
    newreferences = deepcopy(references)
    newreferences = [reference for reference in newreferences if reference != arg]
    return newreferences


external_to_internal_type_mapping = {
    "commonsMedia": "string",
    "entity-schema": "string",
    "external-id": "string",
    "geo-shape": "string",
    "globe-coordinate": "globecoordinate",
    "monolingualtext": "monolingualtext",
    "quantity": "quantity",
    "string": "string",
    "tabular-data": "string",
    "time": "time",
    "url": "string",
    "wikibase-item": "wikibase-entityid",
    "wikibase-property": "wikibase-entityid",
    "math": "string",
    "wikibase-lexeme": "wikibase-entityid",
    "wikibase-form": "wikibase-entityid",
    "wikibase-sense": "wikibase-entityid",
    "musical-notation": "string",
}


@lru_cache
def values_type(prop: i.Pid) -> str:
    """Returns the datatype used internally to store a particular value (distinct from any intended use by Wikibase extensions).

    Args:
        prop: Property to check.

    Returns:
        Internal datatype of the provided property.
    """
    return external_to_internal_type_mapping[values_datatype(prop)]


@lru_cache
def values_datatype(prop: i.Pid) -> str:
    """Finds the specialized datatype (not the underlying one!) of a property.

    Args:
        prop: Property ID to check.

    Returns:
        The outward-facing datatype of the provided property.

    Raises:
        ValueError: if the argument is not a Pid.
    """
    prop_data = tfsl.auth.retrieve_single_entity(prop)
    if i.is_propertydict(prop_data):
        return prop_data["datatype"]
    raise ValueError(f"Attempting to get datatype of non-property {prop}")


def is_novalue(value: Any) -> bool:
    """Checks that a value is a novalue.

    Args:
        value: Value to check.

    Returns:
        Whether the value is a novalue.
    """
    return value is False


def is_somevalue(value: Any) -> bool:
    """Checks that a value is a somevalue.

    Args:
        value: Value to check.

    Returns:
        Whether the value is a somevalue.
    """
    return value is True

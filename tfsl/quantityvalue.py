"""Holder of the QuantityValue class and a function to build one given a JSON representation of it."""

from typing import TYPE_CHECKING

import tfsl.interfaces as i
import tfsl.languages
import tfsl.utils

if TYPE_CHECKING:
    from typing_extensions import TypeGuard


ONE_ITEM = tfsl.utils.prefix_wd("Q199")


class QuantityValue:
    """Representation of a quantity in Wikibase."""

    def __init__(self, amount: float = 0, lowerBound: float = 1, upperBound: float = -1, unit: str = ONE_ITEM) -> None:  # noqa: N803
        """Sets up a QuantityValue.

        Args:
            amount: Basic value for this quantity.
            lowerBound: Lower bound for this quantity.
            upperBound: Upper bound for this quantity.
            unit: Unit for this quantity.
        """
        self.amount: float = amount
        self.lower: float
        self.upper: float
        if lowerBound >= upperBound:
            self.lower = amount
            self.upper = amount
        else:
            self.lower = lowerBound
            self.upper = upperBound
        if not i.is_qid(unit):
            unit = tfsl.utils.strip_prefix_wd(unit)
        self.unit: str = unit

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two quantity values.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this quantity value is equal to the right-hand side.
        """
        if not isinstance(rhs, QuantityValue):
            return NotImplemented
        amts_equal = self.amount == rhs.amount
        lowers_equal = self.lower == rhs.lower
        uppers_equal = self.upper == rhs.upper
        units_equal = self.unit == rhs.unit
        return amts_equal and lowers_equal and uppers_equal and units_equal

    def __hash__(self) -> int:
        """Produces a hash for this QuantityValue.

        Returns:
            Hash for this QuantityValue.
        """
        return hash((self.amount, self.lower, self.upper, self.unit))

    def __str__(self) -> str:
        """Produces a representation of a quantity value intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a QuantityValue.
        """
        if (self.lower == self.amount and self.upper == self.amount):
            value_string = f"{self.amount}"
        else:
            value_string = f"{self.amount}[{self.lower},{self.upper}]"
        unit_string = ""
        if self.unit != "Q199":
            unit_string = f" {self.unit}"
        return value_string + unit_string

    def __jsonout__(self) -> i.QuantityValueDict:
        """Produces a JSON serialization of this QuantityValue.

        Returns:
            JSON serialization of this QuantityValue.
        """
        base_dict: i.QuantityValueDict = {
            "amount": self.amount,
            "unit": tfsl.utils.prefix_wd(self.unit),
        }
        if (self.lower != self.amount or self.upper != self.amount):
            base_dict["lowerBound"] = self.lower
            base_dict["upperBound"] = self.upper
        return base_dict


def is_quantityvalue(value_in: i.ClaimDictValueDictionary) -> "TypeGuard[i.QuantityValueDict]":
    """Checks that the keys expected for a QuantityValue exist.

    Args:
        value_in: JSON serialization of some claim value.

    Returns:
        Whether this has the keys of a QuantityValue serialization.
    """
    return all(key in value_in for key in ["amount", "unit"])


def build_quantityvalue(value_in: i.QuantityValueDict) -> QuantityValue:
    """Builds a QuantityValue given the Wikibase JSON for one.

    Args:
        value_in: JSON representation of a QuantityValue.

    Returns:
        New QuantityValue object.
    """
    return QuantityValue(**value_in)

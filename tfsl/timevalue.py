"""Holder of the TimeValue class and a function to build one given a JSON representation of it."""

import datetime
from functools import singledispatch
from typing import TYPE_CHECKING, Union

if TYPE_CHECKING:
    from typing_extensions import TypeGuard

import tfsl.interfaces as i
import tfsl.languages
import tfsl.utils

PROLEPTIC_GREGORIAN_CALENDAR = tfsl.utils.prefix_wd("Q1985727")


class TimeValue:
    """Representation of a date or time in Wikibase."""

    def __init__(
        self, time: str,
        before: int = 0, after: int = 0, precision: int = 11, timezone: int = 0,
        calendarmodel: str = PROLEPTIC_GREGORIAN_CALENDAR,
    ) -> None:
        """Sets up a TimeValue.

        Args:
            time: Base date/time in question.
            before: Before value for this date.
            after: After value for this date.
            precision: Precision of this date/time.
            timezone: Timezone to be used with this date/time.
            calendarmodel: Calendar to be used with this date/time.
        """
        self.time: str = time
        self.timezone: int = timezone
        self.before: int = before
        self.after: int = after
        self.precision: int = precision
        self.calendarmodel: str = calendarmodel

    def __jsonout__(self) -> i.TimeValueDict:
        """Produces a JSON serialization of this TimeValue.

        Returns:
            JSON serialization of this TimeValue.
        """
        base_dict: i.TimeValueDict = {
            "time": self.time,
            "timezone": self.timezone,
            "before": self.before,
            "after": self.after,
            "precision": self.precision,
            "calendarmodel": self.calendarmodel,
        }
        return base_dict


@singledispatch
def to_timevalue(obj_in: object, precision: int = 11) -> TimeValue:
    """Intended to convert arbitrary objects to TimeValues.

    Args:
        obj_in: Object representing a date of some sort.
        precision: The precision of the date to be returned.

    Returns:
        TimeValue reflecting the content of the provided object and precision.

    Raises:
        ValueError: if the argument cannot be converted to a TimeValue.
    """
    raise ValueError(f"Can't convert {type(obj_in)} to TimeValue")


@to_timevalue.register(datetime.date)
@to_timevalue.register(datetime.datetime)
def _(obj_in: Union[datetime.datetime, datetime.date], precision: int = 11) -> TimeValue:
    """Converts a Python datetime library object to a TimeValue.

    Args:
        obj_in: Object from the datetime library representing a date of some sort.
        precision: The precision of the date to be returned.

    Returns:
        TimeValue reflecting the content of the provided object and precision.

    Raises:
        ValueError: if a precision smaller than day is provided.
    """
    if precision > 11:
        raise ValueError("Precisions smaller than day are not supported (T57755)")
    timestr = obj_in.strftime("+%Y-%m-%dT00:00:00Z") + "/" + str(precision)
    return TimeValue(timestr, precision=precision)


def is_timevalue(value_in: i.ClaimDictValueDictionary) -> "TypeGuard[i.TimeValueDict]":
    """Checks that the keys expected for a TimeValue exist.

    Args:
        value_in: JSON serialization of some claim value.

    Returns:
        Whether this has the keys of a TimeValue serialization.
    """
    return all(key in value_in for key in ["time", "precision"])


def build_timevalue(value_in: i.TimeValueDict) -> TimeValue:
    """Builds a TimeValue given the Wikibase JSON for one.

    Args:
        value_in: JSON representation of a TimeValue.

    Returns:
        New TimeValue object.
    """
    return TimeValue(**value_in)

"""Holds the Lexeme class and a function to build one given a JSON representation of it."""

from textwrap import indent
from typing import (
    Collection, List, Optional, Protocol, Sequence, Union,
    overload,
)

import tfsl.auth
import tfsl.interfaces as i
import tfsl.itemvalue
import tfsl.languages
import tfsl.lexemeform
import tfsl.lexemesense
import tfsl.monolingualtext
import tfsl.monolingualtextholder as mth
import tfsl.statement
import tfsl.statementholder as sth
import tfsl.utils


class LexemeLike(i.MTST, Protocol):
    """Defines methods that may be expected when reading from an object representing a Wikibase lexeme, whether this object is editable (Lexeme) or not (L).

    Attributes:
        id: Identifier for the lexeme.
        lemmata: Lemmata of the lexeme.
        language: Language of the lexeme.
        category: Lexical category of the lexeme.
    """
    @property
    def id(self) -> Optional[i.Lid]:
        """Returns the lexeme's Lid."""
    @property
    def lemmata(self) -> mth.MonolingualTextHolder:
        """Returns the lexeme's lemmata."""
    @property
    def language(self) -> tfsl.languages.Language:
        """Returns the lexeme's language."""
    @property
    def category(self) -> i.Qid:
        """Returns the lexeme's lexical category."""

    def get_forms(self, inflections: Optional[Collection[i.Qid]] = None, exclusions: Optional[Collection[i.Qid]] = None) -> Sequence[tfsl.lexemeform.LexemeFormLike]:
        """Returns those forms on the lexeme with the provided inflectional features, excluding those listed in the exclusions list.

        Args:
            inflections: Any inflections to take into consideration.
            exclusions: Any inflections to exclude from consideration.

        Returns:
            Forms of this lexeme with regard to the inflections and exclusions.
        """

    def get_senses(self) -> Sequence[tfsl.lexemesense.LexemeSenseLike]:
        """Returns the list of senses on the lexeme."""

    def repr_lemmata_first(self) -> str:
        """Provides a string representation of the lexeme where the lemmata precede the lexeme ID."""

    def __jsonout__(self) -> i.LexemeDict:
        """Produces a JSON serialization of this lexeme-like object.

        Returns:
            JSON serialization of this lexeme-like object.
        """

    @overload
    def getitem(self, arg: "tfsl.languages.Language") -> "tfsl.monolingualtext.MonolingualText": ...
    @overload
    def getitem(self, arg: "tfsl.monolingualtext.MonolingualText") -> "tfsl.monolingualtext.MonolingualText": ...
    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...
    @overload
    def getitem(self, key: i.LFid) -> tfsl.lexemeform.LexemeFormLike: ...
    @overload
    def getitem(self, key: i.LSid) -> tfsl.lexemesense.LexemeSenseLike: ...
    @overload
    def getitem(self, key: i.Fid) -> tfsl.lexemeform.LexemeFormLike: ...
    @overload
    def getitem(self, key: i.Sid) -> tfsl.lexemesense.LexemeSenseLike: ...

    def to_editable(self) -> "Lexeme":
        """Returns an editable version of the lexeme."""


class Lexeme:
    """Container for a Wikidata lexeme.

    See the documentation of LexemeLike methods for general information about
    what certain methods do.
    """

    def __init__(
        self,
        lemmata: Union[mth.MonolingualTextHolder, i.MonolingualTextHolderInput],
        lang_in: tfsl.languages.Language,
        cat_in: i.Qid,
        statements: Optional[Union[sth.StatementHolder, i.StatementHolderInput]] = None,
        senses: Optional[i.LexemeSenseList] = None,
        forms: Optional[i.LexemeFormList] = None,
    ) -> None:
        """Sets up a Lexeme.

        Args:
            lemmata: Initial lemmata for this lexeme.
            lang_in: Initial language for this lexeme.
            cat_in: Initial lexical category for this lexeme.
            statements: Initial statements for this lexeme.
            senses: Initial senses for this lexeme.
            forms: Initial forms for this lexeme.
        """
        super().__init__()
        if isinstance(lemmata, mth.MonolingualTextHolder):
            self.lemmata = lemmata
        else:
            self.lemmata = mth.MonolingualTextHolder(lemmata)

        if isinstance(statements, sth.StatementHolder):
            self.statements = statements
        else:
            self.statements = sth.StatementHolder(statements)

        self.language = lang_in
        self.category = cat_in

        if senses is None:
            self.senses = []
        else:
            self.senses = senses

        if forms is None:
            self.forms = []
        else:
            self.forms = forms

        self.pageid: Optional[int] = None
        self.namespace: Optional[int] = None
        self.title: Optional[str] = None
        self.lastrevid: Optional[int] = None
        self.modified: Optional[str] = None
        self.type: Optional[str] = None
        self.id: Optional[i.Lid] = None

    def get_published_settings(self) -> i.LexemePublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the Lexeme JSON dictionary which are only significant at editing time for existing lexemes.
        """
        if self.pageid is not None and self.namespace is not None and self.title is not None and self.lastrevid is not None and self.modified is not None and self.type is not None and self.id is not None:
            return {
                "pageid": self.pageid,
                "ns": self.namespace,
                "title": self.title,
                "lastrevid": self.lastrevid,
                "modified": self.modified,
                "type": self.type,
                "id": self.id,
            }
        return {}

    def set_published_settings(self, lexeme_in: i.LexemePublishedSettings) -> None:
        """Sets based on a Lexeme JSON dictionary those variables which are only significant at editing time for existing lexemes.

        Args:
            lexeme_in: Source statement of relevant editing information.
        """
        if "pageid" in lexeme_in:
            self.pageid = lexeme_in["pageid"]
            self.namespace = lexeme_in["ns"]
            self.title = lexeme_in["title"]
            self.lastrevid = lexeme_in["lastrevid"]
            self.modified = lexeme_in["modified"]
            self.type = lexeme_in["type"]
            self.id = lexeme_in["id"]

    def __add__(self, arg: object) -> "Lexeme":
        """Adds an object to this Lexeme.

        Args:
            arg: Object to add.

        Returns:
            Lexeme reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be added to the Lexeme.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements + arg,
                self.senses, self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.lexemesense.LexemeSense):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements, tfsl.utils.add_to_list(self.senses, arg),
                self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.lexemeform.LexemeForm):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements, self.senses,
                tfsl.utils.add_to_list(self.forms, arg),
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata + arg,
                self.language, self.category, self.statements,
                self.senses, self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        raise NotImplementedError(f"Can't add {type(arg)} to Lexeme")

    def __sub__(self, arg: object) -> "Lexeme":
        """Removes an object from this Lexeme.

        Args:
            arg: Object to remove.

        Returns:
            Lexeme reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from a Lexeme.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements - arg,
                self.senses, self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.lexemesense.LexemeSense):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements,
                tfsl.utils.sub_from_list(self.senses, arg),
                self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.lexemeform.LexemeForm):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata, self.language, self.category,
                self.statements, self.senses,
                tfsl.utils.sub_from_list(self.forms, arg),
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            published_settings = self.get_published_settings()
            lexeme_out = Lexeme(
                self.lemmata - arg,
                self.language, self.category, self.statements,
                self.senses, self.forms,
            )
            lexeme_out.set_published_settings(published_settings)
            return lexeme_out
        raise NotImplementedError(f"Can't subtract {type(arg)} from Lexeme")

    def get_forms(self, inflections: Optional[Collection[i.Qid]] = None, exclusions: Optional[Collection[i.Qid]] = None) -> i.LexemeFormList:
        """(See LexemeLike.get_forms for what this method does.)

        The forms here are returned as LexemeForm objects.

        Args:
            inflections: Any inflections to take into consideration.
            exclusions: Any inflections to exclude from consideration.

        Returns:
            Forms of this lexeme with regard to the inflections and exclusions.
        """
        if inflections is None:
            return self.forms
        initial_form_list = [
            form for form in self.forms
            if all(i in form.features for i in inflections)
        ]
        if exclusions is None:
            return initial_form_list
        return [
            form for form in initial_form_list
            if all(i not in form.features for i in exclusions)
        ]

    def get_senses(self) -> i.LexemeSenseList:
        """(See LexemeLike.get_senses for what this method does.)

        The senses here are returned as LexemeSense objects.

        Returns:
            Senses of this lexeme.
        """
        return self.senses

    def get_language(self) -> tfsl.languages.Language:
        """Gets the lexeme's language.

        Returns:
            Language of the lexeme.
        """
        return self.language

    @overload
    def getitem(self, key: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, key: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, key: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...
    @overload
    def getitem(self, key: i.LFid) -> tfsl.lexemeform.LexemeForm: ...
    @overload
    def getitem(self, key: i.LSid) -> tfsl.lexemesense.LexemeSense: ...
    @overload
    def getitem(self, key: i.Fid) -> tfsl.lexemeform.LexemeForm: ...
    @overload
    def getitem(self, key: i.Sid) -> tfsl.lexemesense.LexemeSense: ...

    def getitem(self, key: object) -> Union[i.StatementList, tfsl.lexemeform.LexemeForm, tfsl.lexemesense.LexemeSense, tfsl.monolingualtext.MonolingualText]:
        """Retrieves a portion of a Lexeme.

        Args:
            key: Indicator of part of a lexeme.

        Returns:
            A list of statements, a form, a sense, or a lemma.

        Raises:
            TypeError: if the key provided can't be reasonably understood as referring to a part of a lexeme.
        """
        if isinstance(key, (tfsl.languages.Language, tfsl.monolingualtext.MonolingualText)):
            return self.lemmata[key]
        elif isinstance(key, str):
            return self.getitem_str(key)
        elif isinstance(key, tfsl.itemvalue.ItemValue):
            return self.getitem_str(key.id)
        raise TypeError(f"Can't get {type(key)} from Lexeme")

    def __getitem__(self, key: object) -> Union[i.StatementList, tfsl.lexemeform.LexemeForm, tfsl.lexemesense.LexemeSense, tfsl.monolingualtext.MonolingualText]:
        """Gets statements, a form, a sense, or a lemma from this Lexeme.

        Args:
            key: Indicator of something to get from a Lexeme.

        Returns:
            Either a list of statements, a form, a sense, or a lemma.
        """
        return self.getitem(key)

    def getitem_pid(self, key: i.Pid) -> i.StatementList:
        """Overload for getitem when the key is a string.

        Args:
            key: Indicator of a part of a lexeme.

        Returns:
            A list of statements.
        """
        return self.statements[key]

    def getitem_fid(self, key: i.LFid) -> tfsl.lexemeform.LexemeForm:
        """Overload for getitem where the key is an LFid.

        Args:
            key: Form ID.

        Returns:
            The form in question.

        Raises:
            KeyError: if no form with the given LFid is found.
        """
        for form in self.forms:
            if form.id == key:
                return form
        raise KeyError

    def getitem_sid(self, key: i.LSid) -> tfsl.lexemesense.LexemeSense:
        """Overload for getitem where the key is an LSid.

        Args:
            key: Sense ID.

        Returns:
            The sense in question.

        Raises:
            KeyError: if no sense with the given LSid is found.
        """
        for sense in self.senses:
            if sense.id == key:
                return sense
        raise KeyError

    def getitem_str(self, key: str) -> Union[i.StatementList, tfsl.lexemeform.LexemeForm, tfsl.lexemesense.LexemeSense]:
        """Overload for getitem where the key is a string.

        Args:
            key: Indicator of a part of a a lexeme.

        Returns:
            A list of statements, a form, or a sense.

        Raises:
            KeyError: if the string can't be understood as a part of a lexeme.
        """
        if i.is_pid(key):
            return self.getitem_pid(key)
        elif i.is_lfid(key):
            return self.getitem_fid(key)
        elif i.is_lsid(key):
            return self.getitem_sid(key)
        elif self.id is not None:
            new_key = "-".join([self.id, key])
            if i.is_lfid(new_key):
                return self.getitem_fid(new_key)
            elif i.is_lsid(new_key):
                return self.getitem_sid(new_key)
        raise KeyError

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the Lexeme.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the Lexeme.
        """
        return self.statements.haswbstatement(property_in, value_in)

    def __str__(self) -> str:
        """Produces a representation of a lexeme intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a Lexeme.

        Todo:
            fix indentation of components
        """
        lemma_str = str(self.lemmata)
        base_str = f": {self.category} in {self.language.item}"

        stmts_str = str(self.statements)

        senses_str = ""
        if len(self.senses) != 0:
            prefix = "{\n"
            suffix = "\n}"
            sense_strings = [str(sense) for sense in self.senses]
            base_str_sense = indent("\n".join(sense_strings), tfsl.utils.DEFAULT_INDENT)
            senses_str = prefix + base_str_sense + suffix

        forms_str = ""
        if len(self.forms) != 0:
            prefix = "(\n"
            suffix = "\n)"
            form_strings = [str(form) for form in self.forms]
            base_str_form = indent("\n".join(form_strings), tfsl.utils.DEFAULT_INDENT)
            forms_str = prefix + base_str_form + suffix

        return "\n".join([lemma_str + base_str, stmts_str, senses_str, forms_str])

    def __repr__(self) -> str:
        """Produces string representation of the Lexeme.

        Returns:
            String representation of the Lexeme.
        """
        return self.id or "L0" + f" ({self.language.item}): " + str(self.lemmata)

    def repr_lemmata_first(self) -> str:
        """Provides a string representation of the lexeme where the lemmata precede the lexeme ID.

        Returns:
            String representation of the Lexeme.
        """
        return str(self.lemmata) + ": " + (self.id or "L0") + f" ({self.language.item})"

    def __jsonout__(self) -> i.LexemeDict:
        """Produces a JSON serialization of this Lexeme.

        Returns:
            JSON serialization of this Lexeme.
        """
        lemma_dict: i.LemmaDictSet = self.lemmata.__jsonout__()

        statement_dict: i.StatementDictSet = self.statements.__jsonout__()

        form_list: List[i.LexemeFormDict] = [form.__jsonout__() for form in self.forms]

        sense_list: List[i.LexemeSenseDict] = [sense.__jsonout__() for sense in self.senses]

        base_dict: i.LexemeDict = {
            "lexicalCategory": self.category,
            "language": self.language.item,
            "type": "lexeme",
            "lemmas": lemma_dict,
            "claims": statement_dict,
            "forms": form_list,
            "senses": sense_list,
        }

        if self.id is not None and self.lastrevid is not None:
            base_dict["id"] = self.id
            base_dict["lastrevid"] = self.lastrevid

        return base_dict

    def to_editable(self) -> "Lexeme":
        """No-op, since a Lexeme is already editable.

        Returns:
            Itself.
        """
        return self


def build_lexeme(lexeme_in: i.LexemeDict) -> Lexeme:
    """Builds a Lexeme from the JSON dictionary describing it.

    Args:
        lexeme_in: JSON representation of a Lexeme.

    Returns:
        New Lexeme object.
    """
    lemmas = mth.build_text_list(lexeme_in["lemmas"])

    lexemecat = lexeme_in["lexicalCategory"]
    language = tfsl.languages.get_first_lang(lexeme_in["language"])

    statements = sth.build_statement_list(lexeme_in["claims"])

    forms = [tfsl.lexemeform.build_form(form) for form in lexeme_in["forms"]]
    senses = [tfsl.lexemesense.build_sense(sense) for sense in lexeme_in["senses"]]

    lexeme_out = Lexeme(lemmas, language, lexemecat, statements, senses, forms)
    lexeme_out.set_published_settings(lexeme_in)
    return lexeme_out


def get_lid(value_in: Union[i.PossibleLexemeReference, tfsl.itemvalue.ItemValue]) -> i.Lid:
    """If the input is an ItemValue, then an ID is retrieved from it and an Lid extracted therefrom; otherwise the extraction is attempted directly on the object.

    Args:
        value_in: Entity to get a lexeme ID from.

    Returns:
        Extracted lexeme ID.
    """
    if isinstance(value_in, tfsl.itemvalue.ItemValue):
        try:
            return value_in.get_lid()
        except TypeError:
            try:
                return i.get_lid_string(value_in.get_lfid())
            except TypeError:
                return i.get_lid_string(value_in.get_lsid())
    else:
        return i.get_lid_string(value_in)


def retrieve_lexeme_json(lid_in: Union[i.PossibleLexemeReference, tfsl.itemvalue.ItemValue]) -> i.LexemeDict:
    """Retrieves the JSON for a single lexeme.

    Args:
        lid_in: Representation of a lexeme ID.

    Returns:
        JSON for that lexeme.

    Raises:
        ValueError: if the returned JSON for an entity is not for a lexeme
    """
    lid = get_lid(lid_in)
    lexeme_dict = tfsl.auth.retrieve_single_entity(lid)
    if i.is_lexemedict(lexeme_dict):
        return lexeme_dict
    raise ValueError(f"Returned JSON for {lid_in} is not a lexeme")


class L:
    """A Lexeme, but lemmata/form representations are not auto-converted to MonolingualTexts, statements are only assembled into Statements when accessed, and forms and senses are returned as LF and LS objects instead of LexemeForms and LexemeSenses.

    See the documentation of LexemeLike methods for general information about
    what certain methods do.

    Attributes:
        id: Identifier for the lexeme.
        lemmata: Lemmata of the lexeme.
        language: Language of the lexeme.
        category: Lexical category of the lexeme.
    """

    def __init__(self, input_arg: Union[int, i.Lid, i.LFid, i.LSid, tfsl.itemvalue.ItemValue]) -> None:
        """Sets up an L.

        Args:
            input_arg: Indicator of a lexeme to retrieve information for.
        """
        self.json: i.LexemeDict = retrieve_lexeme_json(input_arg)

    def to_editable(self) -> Lexeme:
        """Builds an editable Lexeme object.

        Returns:
            Lexeme based on the JSON content of this L.
        """
        return build_lexeme(self.json)

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two lexemes.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this lexeme is equal to the right-hand side.

        Raises:
            ValueError: if the right-hand side is not a lexeme.
        """
        if isinstance(rhs, L):
            return self.json == rhs.json
        elif isinstance(rhs, Lexeme):
            return self.json == rhs.__jsonout__()
        raise ValueError(f"Cannot compare L to {type(rhs)}")

    @property
    def lemmata(self) -> mth.MonolingualTextHolder:
        """(See LexemeLike.lemmata for what this method does.)"""
        return mth.MonolingualTextHolder(mth.build_text_list(self.json["lemmas"]))

    @property
    def category(self) -> i.Qid:
        """(See LexemeLike.category for what this method does.)"""
        return self.json["lexicalCategory"]

    @property
    def language(self) -> tfsl.languages.Language:
        """(See LexemeLike.language for what this method does.)"""
        return self.get_language()

    @property
    def id(self) -> i.Lid:
        """(See LexemeLike.id for what this method does.)"""
        return self.json["id"]

    def __repr__(self) -> str:
        """Produces string representation of the L.

        Returns:
            String representation of the L.
        """
        lemmata = []
        for _, x in self.json["lemmas"].items():
            lemmata.append(x["value"] + "@" + x["language"])
        return self.json["id"] + " (" + self.json["language"] + "): " + " / ".join(lemmata)

    def repr_lemmata_first(self) -> str:
        """Provides a string representation of the lexeme where the lemmata precede the lexeme ID.

        Returns:
            String representation of the Lexeme.
        """
        lemmata = []
        for _, x in self.json["lemmas"].items():
            lemmata.append(x["value"] + "@" + x["language"])
        return " / ".join(lemmata) + ": " + self.json["id"] + " (" + self.json["language"] + ")"

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the item with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return [tfsl.statement.build_statement(stmt) for stmt in self.json["claims"].get(prop, [])]

    def get_forms(self, inflections: Optional[Collection[i.Qid]] = None, exclusions: Optional[Collection[i.Qid]] = None) -> List[tfsl.lexemeform.LF]:
        """(See LexemeLike.get_forms for what this method does.)

        Args:
            inflections: Inflections that should be present on any forms obtained.
            exclusions: Inflections that should not be present on any forms obtained.

        Returns:
            Forms as LF objects.
        """
        if inflections is None:
            return [tfsl.lexemeform.LF(form) for form in self.json["forms"]]
        initial_form_list = [
            tfsl.lexemeform.LF(form) for form in self.json["forms"]
            if all(i in form["grammaticalFeatures"] for i in inflections)
        ]
        if exclusions is None:
            return initial_form_list
        return [
            form for form in initial_form_list
            if all(i not in form.features for i in exclusions)
        ]

    def get_senses(self) -> List[tfsl.lexemesense.LS]:
        """(See LexemeLike.get_senses for what this method does.)

        Returns:
            Senses as LS objects.
        """
        return [tfsl.lexemesense.LS(sense) for sense in self.json["senses"]]

    def get_language(self) -> tfsl.languages.Language:
        """Returns the language of the lexeme.

        Returns:
            Language of the lexeme.
        """
        return tfsl.languages.get_first_lang(self.json["language"])

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the Lexeme.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the Lexeme.
        """
        return sth.haswbstatement(self.json["claims"], property_in, value_in)

    @overload
    def getitem(self, key: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, key: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, key: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...
    @overload
    def getitem(self, key: i.LFid) -> tfsl.lexemeform.LF: ...
    @overload
    def getitem(self, key: i.LSid) -> tfsl.lexemesense.LS: ...
    @overload
    def getitem(self, key: i.Fid) -> tfsl.lexemeform.LF: ...
    @overload
    def getitem(self, key: i.Sid) -> tfsl.lexemesense.LS: ...

    def getitem(self, key: object) -> Union[i.StatementList, tfsl.lexemeform.LF, tfsl.lexemesense.LS, tfsl.monolingualtext.MonolingualText]:
        """Retrieves a portion of an L.

        Args:
            key: Indicator of part of a lexeme.

        Returns:
            A list of statements, a form, a sense, or a lemma.

        Raises:
            TypeError: if the key provided can't be reasonably understood as referring to a part of a lexeme.
        """
        if isinstance(key, tfsl.languages.Language):
            return mth.get_lang_from_mtlist(self.json["lemmas"], key)
        elif isinstance(key, tfsl.monolingualtext.MonolingualText):
            lang = key.language
            lang_code = lang.code
            return tfsl.monolingualtext.build_lemma(self.json["lemmas"][lang_code])
        elif isinstance(key, str):
            return self.getitem_str(key)
        elif isinstance(key, tfsl.itemvalue.ItemValue):
            return self.getitem_str(key.id)
        raise TypeError(f"Can't get {type(key)} from Lexeme")

    def __getitem__(self, key: object) -> Union[i.StatementList, tfsl.lexemeform.LF, tfsl.lexemesense.LS, tfsl.monolingualtext.MonolingualText]:
        """Gets statements, a form, a sense, or a lemma from this Lexeme.

        Args:
            key: Indicator of something to get from a Lexeme.

        Returns:
            Either a list of statements, a form, a sense, or a lemma.
        """
        return self.getitem(key)

    def getitem_pid(self, key: i.Pid) -> i.StatementList:
        """Overload for getitem when the key is a string.

        Args:
            key: Indicator of a part of a lexeme.

        Returns:
            A list of statements.
        """
        return self.get_stmts(key)

    def getitem_fid(self, key: i.LFid) -> tfsl.lexemeform.LF:
        """Overload for getitem where the key is an LFid.

        Args:
            key: Form ID.

        Returns:
            The form in question.

        Raises:
            KeyError: if no form with the given LFid is found.
        """
        for form in self.json["forms"]:
            if form["id"] == key:
                return tfsl.lexemeform.LF(form)
        raise KeyError

    def getitem_sid(self, key: i.LSid) -> tfsl.lexemesense.LS:
        """Overload for getitem where the key is an LSid.

        Args:
            key: Sense ID.

        Returns:
            The sense in question.

        Raises:
            KeyError: if no form with the given LSid is found.
        """
        for sense in self.json["senses"]:
            if sense["id"] == key:
                return tfsl.lexemesense.LS(sense)
        raise KeyError

    def getitem_str(self, key: str) -> Union[i.StatementList, tfsl.lexemeform.LF, tfsl.lexemesense.LS]:
        """Overload for getitem where the key is a string.

        Args:
            key: Indicator of a part of a a lexeme.

        Returns:
            A list of statements, a form, or a sense.

        Raises:
            KeyError: if the string can't be understood as a part of a lexeme.
        """
        if i.is_pid(key):
            return self.getitem_pid(key)
        elif i.is_lfid(key):
            return self.getitem_fid(key)
        elif i.is_lsid(key):
            return self.getitem_sid(key)
        elif self.json["id"] is not None:
            lexeme_id = self.json["id"]
            new_key = "-".join([lexeme_id, key])
            if i.is_lfid(new_key):
                return self.getitem_fid(new_key)
            elif i.is_lsid(new_key):
                return self.getitem_sid(new_key)
        raise KeyError

    def __jsonout__(self) -> i.LexemeDict:
        """Produces a JSON serialization of this L.

        Returns:
            JSON serialization of this L.
        """
        return self.json

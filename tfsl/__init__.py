"""twofivesixlex -- a library to use and edit Wikidata items and lexemes"""

import logging

import tfsl.interfaces as interfaces
import tfsl.utils as utils
from tfsl.auth import WikibaseSession
from tfsl.claim import Claim
from tfsl.coordinatevalue import CoordinateValue
from tfsl.item import Q, Item, ItemLike
from tfsl.itemvalue import ItemValue
from tfsl.languages import Language, langs
from tfsl.lexeme import L, Lexeme, LexemeLike
from tfsl.lexemeform import LF, LexemeForm, LexemeFormLike
from tfsl.lexemesense import LS, LexemeSense, LexemeSenseLike
from tfsl.monolingualtext import MonolingualText
from tfsl.monolingualtextholder import MonolingualTextHolder
from tfsl.property import P, Property
from tfsl.quantityvalue import QuantityValue
from tfsl.reference import Reference
from tfsl.statement import Statement
from tfsl.statementholder import StatementHolder
from tfsl.timevalue import TimeValue

__all__ = ["WikibaseSession", "Claim", "CoordinateValue", "Q", "Item", "ItemLike", "ItemValue", "Language", "langs", "L", "Lexeme", "LexemeLike", "LF", "LexemeForm", "LexemeFormLike", "LS", "LexemeSense", "LexemeSenseLike", "MonolingualText", "MonolingualTextHolder", "P", "Property", "QuantityValue", "Reference", "Statement", "StatementHolder", "TimeValue", "interfaces", "utils"]

logging.getLogger("tfsl").addHandler(logging.NullHandler())

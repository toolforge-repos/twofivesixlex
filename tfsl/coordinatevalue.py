"""Holder of the CoordinateValue class and a function to build one given a JSON representation of it."""

from typing import TYPE_CHECKING, Optional

import tfsl.interfaces as i
import tfsl.languages
import tfsl.utils

if TYPE_CHECKING:
    from typing_extensions import TypeGuard


EARTH_ITEM = tfsl.utils.prefix_wd("Q2")


class CoordinateValue:
    """Representation of a coordinate in Wikibase."""

    def __init__(
        self, latitude: float, longitude: float, precision: float,
        globe: str = EARTH_ITEM, altitude: Optional[float] = None,
    ) -> None:
        """Sets up a CoordinateValue:

        Args:
            latitude: Latitude of this position.
            longitude: Longitude of this position.
            precision: Precision of this position.
            globe: Celestial body on which this position lies.
            altitude: Altitude of the position.
        """
        self.lat: float = latitude
        self.lon: float = longitude
        self.prec: float = precision
        self.alt: Optional[float] = altitude
        self.globe: str = globe

    def __jsonout__(self) -> i.CoordinateValueDict:
        """Produces a JSON serialization of this CoordinateValue.

        Returns:
            JSON serialization of this CoordinateValue.
        """
        base_dict: i.CoordinateValueDict = {
            "latitude": self.lat,
            "longitude": self.lon,
            "altitude": self.alt,
            "precision": self.prec,
            "globe": self.globe,
        }
        return base_dict


def is_coordinatevalue(value_in: i.ClaimDictValueDictionary) -> "TypeGuard[i.CoordinateValueDict]":
    """Checks that the keys expected for a CoordinateValue exist.

    Args:
        value_in: JSON serialization of some claim value.

    Returns:
        Whether this has the keys of a CoordinateValue serialization.
    """
    return all(key in value_in for key in ["latitude", "longitude", "altitude", "precision", "globe"])


def build_coordinatevalue(value_in: i.CoordinateValueDict) -> CoordinateValue:
    """Builds a CoordinateValue given the Wikibase JSON for one.

    Args:
        value_in: JSON representation of a CoordinateValue.

    Returns:
        New CoordinateValue object.
    """
    return CoordinateValue(**value_in)

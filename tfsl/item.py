"""Holds the Item class and a function to build one given a JSON representation of it."""

from typing import Dict, Optional, Protocol, Set, Union, overload

import tfsl.auth
import tfsl.interfaces as i
import tfsl.languages
import tfsl.lexemeform
import tfsl.lexemesense
import tfsl.monolingualtext
import tfsl.monolingualtextholder as mth
import tfsl.statement
import tfsl.statementholder as sth
import tfsl.utils


class ItemLike(i.MTST, Protocol):
    """Defines methods that may be expected when reading from an object representing a Wikibase item, whether this object is editable (Item) or not (Q).

    Attributes:
        id: Identifier for the item.
    """

    def get_label(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the label with the given language code.

        Args:
            lang: Language to get.

        Returns:
            Label of the item in that language.
        """

    def get_description(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the description with the given language code.

        Args:
            lang: Language to get.

        Returns:
            Description of the item in that language.
        """

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the item with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """

    @property
    def id(self) -> Optional[i.Qid]:
        """Returns the item's Qid."""

    def __jsonout__(self) -> i.ItemDict:
        """Produces a JSON serialization of this item-like object.

        Returns:
            JSON serialization of this item-like object.
        """

    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...


class Item:
    """Container for a Wikidata item."""

    def __init__(
        self,
        labels: Optional[Union[mth.MonolingualTextHolder, i.MonolingualTextList]] = None,
        descriptions: Optional[Union[mth.MonolingualTextHolder, i.MonolingualTextList]] = None,
        aliases: Optional[Dict[i.LanguageCode, Set[str]]] = None,
        statements: Optional[Union[sth.StatementHolder, i.StatementHolderInput]] = None,
        sitelinks: Optional[Dict[str, i.SitelinkDict]] = None,
    ) -> None:
        """Sets up an Item.

        Args:
            labels: Initial labels for this item.
            descriptions: Initial descriptions for this item.
            aliases: Initial aliases for this item.
            statements: Initial statements for this item.
            sitelinks: Initial sitelinks for this item.
        """
        super().__init__()
        if isinstance(labels, mth.MonolingualTextHolder):
            self.labels = labels
        else:
            self.labels = mth.MonolingualTextHolder(labels)

        if isinstance(descriptions, mth.MonolingualTextHolder):
            self.descriptions = descriptions
        else:
            self.descriptions = mth.MonolingualTextHolder(descriptions)

        if aliases is None:
            self.aliases = {}
        else:
            self.aliases = aliases if isinstance(aliases, dict) else dict(aliases)

        if isinstance(statements, sth.StatementHolder):
            self.statements = statements
        else:
            self.statements = sth.StatementHolder(statements)

        if sitelinks is None:
            self.sitelinks = {}
        else:
            self.sitelinks = sitelinks if isinstance(sitelinks, dict) else dict(sitelinks)

        self.pageid: Optional[int] = None
        self.namespace: Optional[int] = None
        self.title: Optional[str] = None
        self.lastrevid: Optional[int] = None
        self.modified: Optional[str] = None
        self.type: Optional[str] = None
        self.id: Optional[i.Qid] = None

    def get_published_settings(self) -> i.ItemPublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the Item JSON dictionary which are only significant at editing time for existing items.
        """
        if self.pageid is not None and self.namespace is not None and self.title is not None and self.lastrevid is not None and self.modified is not None and self.type is not None and self.id is not None:
            return {
                "pageid": self.pageid,
                "ns": self.namespace,
                "title": self.title,
                "lastrevid": self.lastrevid,
                "modified": self.modified,
                "type": self.type,
                "id": self.id,
            }
        return {}

    def set_published_settings(self, item_in: i.ItemPublishedSettings) -> None:
        """Sets based on an Item JSON dictionary those variables which are only significant at editing time for existing items.

        Args:
            item_in: Source statement of relevant editing information.
        """
        if "pageid" in item_in:
            self.pageid = item_in["pageid"]
            self.namespace = item_in["ns"]
            self.title = item_in["title"]
            self.lastrevid = item_in["lastrevid"]
            self.modified = item_in["modified"]
            self.type = item_in["type"]
            self.id = item_in["id"]

    def __getitem__(self, key: object) -> i.StatementList:
        """Gets statements from this Item.

        Args:
            key: Ideally a property ID.

        Returns:
            List of statements with that property.

        Raises:
            KeyError: if the argument is not a property ID.
        """
        if isinstance(key, str):
            if i.is_pid(key):
                return self.statements[key]
            else:
                raise KeyError
        raise KeyError

    def __str__(self) -> str:
        """Produces a representation of an item intended for printing to a terminal (not a serialization).

        Returns:
            String representation of an Item.
        """
        remainder = f"{len(self.labels)}/{len(self.descriptions)}, {len(self.statements)}"
        if self.id:
            return "{" + self.id + ": " + remainder + "}"
        return "{Item: " + remainder + "}"

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the Item.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the Item.
        """
        return self.statements.haswbstatement(property_in, value_in)

    def get_label(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Returns the label on the Item with the provided language.

        Args:
            arg: Language to get.

        Returns:
            Label of the item in that language.
        """
        return self.labels[arg]

    def get_description(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Returns the description on the Item with the provided language.

        Args:
            arg: Language to get.

        Returns:
            Description of the item in that language.
        """
        return self.descriptions[arg]

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Returns a list of statements for the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return self[prop]

    def __add__(self, arg: object) -> "Item":
        """Adds an object to this Item.

        Args:
            arg: Object to add.

        Returns:
            Item reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be added to the Item.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            item_out = Item(
                self.labels, self.descriptions, self.aliases,
                self.statements + arg, self.sitelinks,
            )
            item_out.set_published_settings(published_settings)
            return item_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            raise NotImplementedError("Adding MonolingualText to Item is ambiguous")
        raise NotImplementedError(f"Can't add {type(arg)} to Item")

    def add_label(self, arg: tfsl.monolingualtext.MonolingualText) -> "Item":
        """Adds the provided MonolingualText as a label to the Item, overwriting any existing label in that MonolingualText's language.

        Args:
            arg: MonolingualText to add.

        Returns:
            Item with the noted language label added.
        """
        published_settings = self.get_published_settings()
        item_out = Item(
            self.labels + arg, self.descriptions, self.aliases,
            self.statements, self.sitelinks,
        )
        item_out.set_published_settings(published_settings)
        return item_out

    def add_description(self, arg: tfsl.monolingualtext.MonolingualText) -> "Item":
        """Adds the provided MonolingualText as a description to the Item, overwriting any existing description in that MonolingualText's language.

        Args:
            arg: MonolingualText to add.

        Returns:
            Item with the noted language description added.
        """
        published_settings = self.get_published_settings()
        item_out = Item(
            self.labels, self.descriptions + arg, self.aliases,
            self.statements, self.sitelinks,
        )
        item_out.set_published_settings(published_settings)
        return item_out

    def __sub__(self, arg: object) -> "Item":
        """Removes an object from this Item.

        Args:
            arg: Object to remove.

        Returns:
            Item reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from an Item.
        """
        if isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            item_out = Item(
                self.labels, self.descriptions, self.aliases,
                self.statements - arg, self.sitelinks,
            )
            item_out.set_published_settings(published_settings)
            return item_out
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            raise NotImplementedError("Subtracting MonolingualText from Item is ambiguous")
        raise NotImplementedError(f"Can't subtract {type(arg)} from Lexeme")

    def sub_label(self, arg: i.LanguageOrMT) -> "Item":
        """Removes the label with the provided language (or the language of the provided MonolingualText) from the Item.

        Args:
            arg: Language or MonolingualText (whose language will be used).

        Returns:
            Item with the noted language label removed.
        """
        published_settings = self.get_published_settings()
        item_out = Item(
            self.labels - arg, self.descriptions, self.aliases,
            self.statements, self.sitelinks,
        )
        item_out.set_published_settings(published_settings)
        return item_out

    def sub_description(self, arg: i.LanguageOrMT) -> "Item":
        """Removes the description with the provided language (or the language of the provided MonolingualText) from the Item.

        Args:
            arg: Language or MonolingualText (whose language will be used).

        Returns:
            Item with the noted language description removed.
        """
        published_settings = self.get_published_settings()
        item_out = Item(
            self.labels, self.descriptions - arg, self.aliases,
            self.statements, self.sitelinks,
        )
        item_out.set_published_settings(published_settings)
        return item_out

    def __jsonout__(self) -> i.ItemDict:
        """Produces a JSON serialization of this Item.

        Returns:
            JSON serialization of this Item.
        """
        labels_dict = self.labels.__jsonout__()
        descriptions_dict = self.descriptions.__jsonout__()

        statement_dict: i.StatementDictSet = self.statements.__jsonout__()

        base_dict: i.ItemDict = {
            "labels": labels_dict,
            "descriptions": descriptions_dict(),
            "aliases": self.aliases,
            "claims": statement_dict,
            "sitelinks": self.sitelinks,
        }
        if self.id is not None and self.lastrevid is not None:
            base_dict["id"] = self.id
            base_dict["lastrevid"] = self.lastrevid

        return base_dict


def build_item(item_in: i.ItemDict) -> Item:
    """Builds an Item from the JSON dictionary describing it.

    Args:
        item_in: JSON representation of an Item.

    Returns:
        New Item object.
    """
    labels = mth.build_text_list(item_in["labels"])
    descriptions = mth.build_text_list(item_in["descriptions"])
    statements = sth.build_statement_list(item_in["claims"])

    aliases: Dict[i.LanguageCode, Set[str]] = {}
    for lang, aliaslist in item_in["aliases"].items():
        aliases[lang] = set()
        for alias in aliaslist:
            new_alias = alias["value"]  # @ tfsl.languages.get_first_lang(alias["language"])
            aliases[lang].add(new_alias)

    sitelinks = item_in.get("sitelinks", {})

    item_out = Item(labels, descriptions, aliases, statements, sitelinks)
    item_out.set_published_settings(item_in)
    return item_out


def retrieve_item_json(qid_in: Union[int, i.Qid]) -> i.ItemDict:
    """Retrieves the JSON for the item with the given Qid.

    Args:
        qid_in: Representation of an item ID.

    Returns:
        JSON for that item.

    Raises:
        ValueError: if the returned JSON for the given Qid is not for an Item.
    """
    qid = i.get_qid_string(qid_in)
    item_dict = tfsl.auth.retrieve_single_entity(qid)
    if i.is_itemdict(item_dict):
        return item_dict
    raise ValueError(f"Returned JSON for {qid_in} is not an item")


class Q:
    """An Item, but labels/descriptions are not auto-converted to MonolingualTexts and statements are only assembled into Statements when accessed.

    Attributes:
        id: Identifier for the item.
    """

    def __init__(self, input_arg: Union[int, i.Qid, i.ItemDict]) -> None:
        """Sets up a Q.

        Args:
            input_arg: Either an indicator of an item ID or the JSON for an item.
        """
        self.json: i.ItemDict
        if i.is_itemdict(input_arg):
            self.json = input_arg
        else:
            self.json = retrieve_item_json(input_arg)

    @property
    def id(self) -> i.Qid:
        """(See ItemLike.id for what this method does.)"""
        return self.json["id"]

    def get_label(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the label with the given language code.

        Args:
            lang: Language to find.

        Returns:
            MonolingualText with the item's label in the given language.
        """
        label_dict: i.LemmaDict = self.json["labels"][lang.code]
        return label_dict["value"] @ lang

    def get_description(self, lang: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText:
        """Assembles a MonolingualText containing the description with the given language code.

        Args:
            lang: Language to find.

        Returns:
            MonolingualText with the item's description in the given language.
        """
        description_dict: i.LemmaDict = self.json["descriptions"][lang.code]
        return description_dict["value"] @ lang

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the item with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return [tfsl.statement.build_statement(stmt) for stmt in self.json["claims"].get(prop, [])]

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the Item.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the Item.
        """
        return sth.haswbstatement(self.json["claims"], property_in, value_in)

    @overload
    def getitem(self, key: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...

    def getitem(self, key: object) -> i.StatementList:
        """Retrieves a portion of an Item.

        Args:
            key: Indicator of part of an item.

        Returns:
            A list of statements.

        Raises:
            TypeError: if the key provided can't be reasonably understood as referring to a part of an item.
        """
        if isinstance(key, str):
            return self.getitem_str(key)
        elif isinstance(key, tfsl.itemvalue.ItemValue):
            return self.getitem_str(key.id)
        raise TypeError(f"Can't get {type(key)} from Lexeme")

    def __getitem__(self, key: object) -> i.StatementList:
        """Gets statements from this Item.

        Args:
            key: Ideally a property ID.

        Returns:
            List of statements with that property.
        """
        return self.getitem(key)

    def getitem_pid(self, key: i.Pid) -> i.StatementList:
        """Overload for getitem when the key is a property ID.

        Args:
            key: Indicator of a part of an item.

        Returns:
            A list of statements.
        """
        return self.get_stmts(key)

    def getitem_str(self, key: str) -> i.StatementList:
        """Overload for getitem when the key is a string.

        Args:
            key: Indicator of a part of an item.

        Returns:
            A list of statements.

        Raises:
            KeyError: if the key is not a Pid.
        """
        if i.is_pid(key):
            return self.getitem_pid(key)
        raise KeyError

    def __jsonout__(self) -> i.ItemDict:
        """Produces a JSON serialization of this Q.

        Returns:
            JSON serialization of this Q.
        """
        return self.json

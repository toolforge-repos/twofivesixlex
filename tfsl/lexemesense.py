"""Holds the LexemeSense class and a function to build one given a JSON representation of it."""

from functools import singledispatchmethod
from typing import Optional, Protocol, Union, overload

import tfsl.interfaces as i
import tfsl.monolingualtext
import tfsl.monolingualtextholder as mth
import tfsl.statement
import tfsl.statementholder as sth
import tfsl.utils


class LexemeSenseLike(i.MTST, Protocol):
    """Defines methods that may be expected when reading from an object representing a Wikibase lexeme sense, whether this object is editable (LexemeSense) or not (LS).

    Attributes:
        glosses: Sense glosses of the lexeme sense.
        id: Identifer for the lexeme sense.
    """
    @property
    def glosses(self) -> mth.MonolingualTextHolder:
        """Returns the sense's glosses."""
    @property
    def id(self) -> Optional[str]:
        """Returns the sense's LSid."""

    def to_editable(self) -> "LexemeSense":
        """Returns an editable version of the sense."""

    def __jsonout__(self) -> i.LexemeSenseDict:
        """Produces a JSON serialization of this sense-like object.

        Returns:
            JSON serialization of this sense-like object.
        """


class LexemeSense:
    """Container for a Wikidata lexeme sense."""

    def __init__(
        self,
        glosses: Union[
            mth.MonolingualTextHolder,
            tfsl.monolingualtext.MonolingualText,
            i.MonolingualTextList,
        ],
        statements: Optional[Union[sth.StatementHolder, i.StatementHolderInput]] = None,
    ) -> None:
        """Sets up a LexemeSense.

        Args:
            glosses: Initial sense glosses.
            statements: Initial sense statements.
        """
        super().__init__()
        if isinstance(glosses, mth.MonolingualTextHolder):
            self.glosses = glosses
        else:
            self.glosses = mth.MonolingualTextHolder(glosses)

        if isinstance(statements, sth.StatementHolder):
            self.statements = statements
        else:
            self.statements = sth.StatementHolder(statements)

        self.id: Optional[str] = None

    def get_published_settings(self) -> i.LexemeSensePublishedSettings:
        """Obtains information relevant when submitting changes to Wikidata.

        Returns:
            Dictionary containing those portions of the LexemeSense JSON dictionary which are only significant at editing time for existing lexeme senses.
        """
        if self.id is not None:
            return {
                "id": self.id,
            }
        return {}

    def set_published_settings(self, sense_in: i.LexemeSensePublishedSettings) -> None:
        """Sets based on a LexemeSense JSON dictionary those variables which are only significant at editing time for existing lexeme senses.

        Args:
            sense_in: Source statement of relevant editing information.
        """
        if "id" in sense_in:
            self.id = sense_in["id"]

    @overload
    def getitem(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...

    def getitem(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Retrieves a portion of a LexemeSense.

        Args:
            arg: Indicator of part of a lexeme sense.

        Returns:
            A list of statements or a gloss.

        Raises:
            KeyError: if the key provided can't be reasonably understood as referring to a part of a lexeme sense.
        """
        if isinstance(arg, str):
            return self.statements[arg]
        elif isinstance(arg, tfsl.itemvalue.ItemValue):
            return self.statements[arg.id]
        elif isinstance(arg, (tfsl.languages.Language, tfsl.monolingualtext.MonolingualText)):
            return self.glosses[arg]
        raise KeyError(f"Can't get {type(arg)} from LexemeForm")

    def __getitem__(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Gets statements or a gloss from this LexemeSense.

        Args:
            arg: Indicator of something to get from a LexemeSense.

        Returns:
            Either a list of statements or a gloss.
        """
        return self.getitem(arg)

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the LexemeSense.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the LexemeSense.
        """
        return self.statements.haswbstatement(property_in, value_in)

    def __add__(self, arg: object) -> "LexemeSense":
        """Adds an object to this LexemeSense.

        Args:
            arg: Object to add.

        Returns:
            LexemeSense reflecting the added object.

        Raises:
            NotImplementedError: if the object cannot be added to the LexemeSense.
        """
        if isinstance(arg, tfsl.monolingualtext.MonolingualText):
            published_settings = self.get_published_settings()
            sense_out = LexemeSense(self.glosses + arg, self.statements)
            sense_out.set_published_settings(published_settings)
            return sense_out
        elif isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            sense_out = LexemeSense(self.glosses, self.statements + arg)
            sense_out.set_published_settings(published_settings)
            return sense_out
        raise NotImplementedError(f"Can't add {type(arg)} to LexemeSense")

    def __sub__(self, arg: object) -> "LexemeSense":
        """Removes an object from this LexemeSense.

        Args:
            arg: Object to remove.

        Returns:
            LexemeSense reflecting the removed object.

        Raises:
            NotImplementedError: if the object cannot be subtracted from a LexemeSense.
        """
        if isinstance(arg, (tfsl.languages.Language, tfsl.monolingualtext.MonolingualText)):
            published_settings = self.get_published_settings()
            sense_out = LexemeSense(self.glosses - arg, self.statements)
            sense_out.set_published_settings(published_settings)
            return sense_out
        elif isinstance(arg, str):
            published_settings = self.get_published_settings()
            if i.is_pid(arg):
                sense_out = LexemeSense(self.glosses, self.statements - arg)
            sense_out.set_published_settings(published_settings)
            return sense_out
        elif isinstance(arg, tfsl.statement.Statement):
            published_settings = self.get_published_settings()
            sense_out = LexemeSense(self.glosses, self.statements - arg)
            sense_out.set_published_settings(published_settings)
            return sense_out
        raise NotImplementedError(f"Can't subtract {type(arg)} from LexemeSense")

    def __contains__(self, arg: object) -> bool:
        """Checks for something inside the LexemeSense.

        Args:
            arg: Object to check for.

        Returns:
            Whether the object is inside the LexemeSense.
        """
        return self.contains(arg)

    @singledispatchmethod
    def contains(self, arg: object) -> bool:
        """Dispatches __contains__.

        Args:
            arg: Something to check for.

        Returns:
            Whether that object is in the LexemeSense.

        Raises:
            KeyError: if the argument can't be checked for in a LexemeSense.
        """
        raise KeyError(f"Can't check for {type(arg)} in LexemeSense")

    @contains.register(tfsl.languages.Language)
    @contains.register(tfsl.monolingualtext.MonolingualText)
    def _(self, arg: i.LanguageOrMT) -> bool:
        """Checks for a language in the sense's glosses.

        Args:
            arg: Language that (or MonolingualText whose language) will be checked for.

        Returns:
            Whether the sense has a gloss in that language.
        """
        return arg in self.glosses

    @contains.register(tfsl.statement.Statement)
    @contains.register(tfsl.claim.Claim)
    @contains.register(str)
    def _(self, arg: Union[str, tfsl.claim.Claim, tfsl.statement.Statement]) -> bool:
        """Checks for a property in the sense's statements.

        Args:
            arg: Property ID (or a Claim or Statement whose property will be used).

        Returns:
            Whether the property is in the sense's statements.
        """
        return arg in self.statements

    def __repr__(self) -> str:
        """Produces string representation of the LexemeSense.

        Returns:
            String representation of the LexemeSense.
        """
        return f"<{self.id}: {len(self.glosses)} glosses, {len(self.statements)} statements>"

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two lexeme senses.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this lexeme sense is equal to the right-hand side.
        """
        if not isinstance(rhs, LexemeSense):
            return NotImplemented
        glosses_equal = self.glosses == rhs.glosses
        statements_equal = self.statements == rhs.statements
        return glosses_equal and statements_equal

    def __str__(self) -> str:
        """Produces a representation of a lexeme sense intended for printing to a terminal (not a serialization).

        Returns:
            String representation of a LexemeSense.
        """
        gloss_str = str(self.glosses)
        stmt_str = str(self.statements)
        return "\n".join([gloss_str, stmt_str])

    def __jsonout__(self) -> i.LexemeSenseDict:
        """Produces a JSON serialization of this LexemeSense.

        Returns:
            JSON serialization of this LexemeSense.
        """
        glosses_dict = self.glosses.__jsonout__()
        base_dict: i.LexemeSenseDict = {"glosses": glosses_dict}

        if self.id is not None:
            base_dict["id"] = self.id
        else:
            base_dict["add"] = ""

        if (statement_dict := self.statements.__jsonout__()):
            base_dict["claims"] = statement_dict
        else:
            base_dict["claims"] = {}

        return base_dict

    def to_editable(self) -> "LexemeSense":
        """No-op, since a LexemeSense is already editable.

        Returns:
            Itself.
        """
        return self


def build_sense(sense_in: i.LexemeSenseDict) -> LexemeSense:
    """Builds a LexemeSense from the JSON dictionary describing it.

    Args:
        sense_in: JSON representation of a LexemeSense.

    Returns:
        New LexemeSense object.
    """
    glosses = mth.build_text_list(sense_in["glosses"])
    statements = sth.build_statement_list(sense_in["claims"])

    sense_out = LexemeSense(glosses, statements)
    sense_out.set_published_settings(sense_in)
    return sense_out


class LS:
    """A LexemeSense, but sense glosses are not auto-converted to MonolingualTexts and statements are only assembled into Statements when accessed.

    See the documentation of LexemeSenseLike methods for general information about
    what certain methods do.

    Attributes:
        glosses: Sense glosses of the lexeme sense.
        id: Identifer for the lexeme sense.
    """

    def __init__(self, sense_json: i.LexemeSenseDict) -> None:
        """Sets up an LS.

        Args:
            sense_json: JSON for this lexeme sense.
        """
        self.json = sense_json

    def to_editable(self) -> LexemeSense:
        """Builds an editable LexemeSense object.

        Returns:
            LexemeSense based on the JSON content of this LS.
        """
        return build_sense(self.json)

    def __eq__(self, rhs: object) -> bool:
        """Checks for equality of two LS'es.

        Args:
            rhs: Right-hand side of the equality check.

        Returns:
            Whether this LS is equal to the right-hand side.

        Raises:
            ValueError: if the right-hand side cannot be compared to an LS.
        """
        if isinstance(rhs, LS):
            return self.json == rhs.json
        elif isinstance(rhs, LexemeSense):
            return self.json == rhs.__jsonout__()
        raise ValueError(f"Cannot compare LS to {type(rhs)}")

    @property
    def id(self) -> i.LSid:
        """(See LexemeSenseLike.id for what this method does.)"""
        current_id = self.json["id"]
        if i.is_lsid(current_id):
            return current_id
        raise ValueError("Somehow the sense ID is not a valid sense ID")

    @property
    def glosses(self) -> mth.MonolingualTextHolder:
        """(See LexemeSenseLike.glosses for what this method does.)"""
        return mth.MonolingualTextHolder(mth.build_text_list(self.json["glosses"]))

    def __repr__(self) -> str:
        """Produces string representation of the LS.

        Returns:
            String representation of the LS.
        """
        return f"<{self.id}: {len(self.json['glosses'])} glosses, {sum(len(y) for x, y in self.json['claims'].items())} statements>"

    def haswbstatement(self, property_in: i.Pid, value_in: Optional[i.ClaimValue] = None) -> bool:
        """Checks for a property (with an optional value) on the LexemeSense.

        Shamelessly named after the keyword used on Wikidata to look for a statement.

        Args:
            property_in: Property to check for.
            value_in: Value to check for.

        Returns:
            Whether the property (with the value provided) exists on the LexemeSense.
        """
        return sth.haswbstatement(self.json["claims"], property_in, value_in)

    @overload
    def getitem(self, arg: tfsl.languages.Language) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: tfsl.monolingualtext.MonolingualText) -> tfsl.monolingualtext.MonolingualText: ...
    @overload
    def getitem(self, arg: i.Pid) -> i.StatementList: ...
    @overload
    def getitem(self, arg: "tfsl.itemvalue.ItemValue") -> i.StatementList: ...

    def getitem(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Retrieves a portion of an LS.

        Args:
            arg: Indicator of part of a lexeme sense.

        Returns:
            A list of statements or a gloss.

        Raises:
            KeyError: if the key provided can't be reasonably understood as referring to a part of a lexeme sense.
        """
        if isinstance(arg, str):
            return self.getitem_str(arg)
        elif isinstance(arg, tfsl.itemvalue.ItemValue):
            return self.getitem_str(arg.id)
        elif isinstance(arg, tfsl.languages.Language):
            return mth.get_lang_from_mtlist(self.json["glosses"], arg)
        elif isinstance(arg, tfsl.monolingualtext.MonolingualText):
            lang = arg.language
            lang_code = lang.code
            return tfsl.monolingualtext.build_lemma(self.json["glosses"][lang_code])
        raise KeyError(f"Can't get {type(arg)} from LexemeSense")

    def __getitem__(self, arg: object) -> Union[i.StatementList, tfsl.monolingualtext.MonolingualText]:
        """Gets statements or a gloss from this LexemeSense.

        Args:
            arg: Indicator of something to get from a LexemeSense.

        Returns:
            Either a list of statements or a gloss.
        """
        return self.getitem(arg)

    def getitem_pid(self, key: i.Pid) -> i.StatementList:
        """Overload for getitem when the key is a property ID.

        Args:
            key: Indicator of a part of a lexeme sense.

        Returns:
            A list of statements.
        """
        return self.get_stmts(key)

    def get_stmts(self, prop: i.Pid) -> i.StatementList:
        """Assembles a list of Statements present on the item with the given property.

        Args:
            prop: Property to find.

        Returns:
            List of statements with that property.
        """
        return [tfsl.statement.build_statement(stmt) for stmt in self.json["claims"].get(prop, [])]

    def getitem_str(self, key: str) -> i.StatementList:
        """Common handling of __getitem__ for inputs as strings or the ids of ItemValues.

        Args:
            key: Indicator of a part of a LexemeSense.

        Returns:
            A list of statements.

        Raises:
            KeyError: if the argument is not a Pid.
        """
        if i.is_pid(key):
            return self.getitem_pid(key)
        raise KeyError

    def __jsonout__(self) -> i.LexemeSenseDict:
        """Produces a JSON serialization of this LS.

        Returns:
            JSON serialization of this LS.
        """
        return self.json

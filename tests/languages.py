"""Tests functionality from the tfsl.languages module."""

import unittest

from tfsl.languages import langs


class TestLanguageMethods(unittest.TestCase):
    """Test case for tfsl.Language methods."""

    def setUp(self) -> None:
        """Sets up the tests in this TestCase."""
        self.language = langs.bn_
        self.language2 = langs.ctg_
        self.language3 = langs.bn_

    def test_string_out(self) -> None:
        """Tests the string representation of a Language."""
        string_out = f"{self.language.code} ({self.language.item})"
        self.assertEqual(str(self.language), string_out)

    def test_compare_langs(self) -> None:
        """Tests the comparison of multiple Languages."""
        self.assertEqual(self.language3, self.language)
        self.assertNotEqual(self.language2, self.language)

        self.assertEqual(self.language, "bn")
        self.assertNotEqual(self.language, "ctg")
        self.assertEqual(self.language2, "ctg")

        self.assertEqual(self.language, "Q9610")
        self.assertNotEqual(self.language, "Q33173")
        self.assertEqual(self.language2, "Q33173")


if __name__ == "__main__":
    unittest.main()

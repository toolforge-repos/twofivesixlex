"""Tests functionality from the tfsl.claim module."""

import unittest

from tfsl.claim import Claim
from tfsl.languages import langs


class TestClaimMethods(unittest.TestCase):
    """Test case for tfsl.Claim methods."""

    def setUp(self) -> None:
        """Sets up the tests in this TestCase."""
        self.property = "P1476"
        self.value_mt = "চাকা" @ langs.bn_

    def test_claim_mt_direct(self) -> None:
        """Tests the direct creation of a Claim with a specified value."""
        x = Claim(self.property, self.value_mt)
        self.assertEqual(x.property, self.property)
        self.assertEqual(x.value, self.value_mt)

    def test_claim_novalue(self) -> None:
        """Tests the direct creation of a Claim with novalue."""
        x = Claim(self.property, False)
        self.assertEqual(x.property, self.property)
        self.assertFalse(x.value)

    def test_claim_somevalue(self) -> None:
        """Tests the direct creation of a Claim with somevalue."""
        x = Claim(self.property, True)
        self.assertEqual(x.property, self.property)
        self.assertIsInstance(x.value, bool)
        self.assertTrue(x.value)


if __name__ == "__main__":
    unittest.main()

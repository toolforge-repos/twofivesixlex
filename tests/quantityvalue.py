"""Tests functionality from the tfsl.quantityvalue module."""

import unittest


class TestQuantityValueMethods(unittest.TestCase):
    """Test case for tfsl.QuantityValue methods."""

    def setUp(self) -> None:
        """Sets up the tests in this TestCase."""
        self._positivemagnitude = 10
        self._negativemagnitude = -10
        self._tolerance = 0.5
        self._unit = "Q174728"
